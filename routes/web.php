<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome')->name('welcome');


Route::get('/logowanie', function () {
    return view('logowanie');
});

Route::get('/rejestracja', function () {
    return view('rejestracja');
});

Route::get('/wylogowanie', function () {
    return view('wylogowanie');
});



Route::get('/nonclimachat', function () {
    return view('nonclimachat');
});





Route::get('/przygody', function () {
    return view('przygody');
});


Route::get('/biblioteka', function () {
    return view('biblioteka');
});


Route::get('/karczma', function () {
    return view('karczma');
});


Route::get('/listagraczy', function () {
    return view('listagraczy');
});


Route::get('/wydarzenia', function () {
    return view('wydarzenia');
});



Route::get('/listapostaci', function () {
    return view('listapostaci');
});



    Route::get('/nowapostac', function () {
        return view('nowapostac');
});


Route::get('/listagraczy', function () {
    return view('listagraczy');
});


Route::post('newpost', 'newPostController@store');
Route::get('newpost', 'newPostController@create');




Route::get('/register', 'RegistrationController@create');
Route::post('register', 'RegistrationController@store');
 
Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::post('przygody', 'PrzygodyController@store');
Route::post('biblioteka', 'BibliotekaController@store');
Route::post('wydarzenia', 'WydarzeniaController@store');
Route::post('listapostaci', 'ListaPostaciController@store');
Route::post('listagraczy', 'ListaGraczyController@store');
Route::post('nowapostac', 'NewCharacterController@store');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/przygody', 'PrzygodyController@index')->name('przygody');
Route::get('/biblioteka', 'BibliotekaController@index')->name('biblioteka');
Route::get('/wydarzenia', 'WydarzeniaController@index')->name('wydarzenia');
Route::get('/listapostaci', 'ListaPostaciController@index')->name('listapostaci');
Route::get('/listagraczy', 'ListaGraczyController@index')->name('listagraczy');
Route::get('karczma', 'KarczmaController@index')->name('listagraczy');



Route::get('/nowapostac', 'NewCharacterController@index')->name('nowapostac');
Route::get('/chat', 'ChatsController@index');
Route::get('/messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage');


Route::get('/gracz/{id}', 'ListaGraczyController@getuser')->name('getuser');
Route::post('/gracz/{id}', 'ListaGraczyController@getuser');





Route::get('/temat/{id}', 'OdpowiedziController@index')->name('gettopic');  //!!! zrobione
Route::post('/temat/{id}', 'OdpowiedziController@index');                   //!!! do zrobienia









Route::get('usuwanieposta/{id}', 'PrzygodyController@destroyClient')->name('usuwanieposta');  //zrobiobne
Route::get('usuwaniepostaci/{id}', 'ListaPostaciController@destroyPostac')->name('usuwaniepostaci');  //zrobione
Route::get('usuwaniegracza/{id}', 'ListaGraczyController@destroyGracz')->name('usuwaniegracza'); //zrobione







 Route::get('edytujpostac/{id}', 'ListaPostaciController@edytujPostac')->name('edytujpostac');  //!!!!  zrobione - tylko rasaPostaci nie działa
 Route::get('edytujgracza/{id}', 'ListaGraczyController@edytujGracz')->name('edytujgracza');


 Route::post('edytujpostac/{id}', 'ListaPostaciController@edytujPostacZapisz')->name('edytujpostaczapisz');  //!!!! teraz robie




 Route::get('edytujposta/{id}', 'PrzygodyController@edytujPosta')->name('edytujposta');  //
 Route::get('edytujposta/{id}', 'WydarzeniaController@edytujPosta')->name('edytujposta');  // teraz robie
//  Route::get('edytujposta/{id}', 'BibliotekaController@edytujPosta')->name('edytujposta');  //
//  Route::get('edytujposta/{id}', 'HomeController@edytujPosta')->name('edytujposta');  // 


 Route::post('edytujposta/{id}', 'PrzygodyController@edytujPostacZapisz')->name('edytujpostazapisz');  //!!!! 
 Route::post('edytujposta/{id}', 'WydarzeniaController@edytujPostaZapisz')->name('edytujpostazapisz');  //!!!! teraz robie








 Route::get('/nowaodpowiedz/{id_posta}', 'OdpowiedziController@nowaodpowiedz')->name('nowaodpowiedz');  //!!! zrobione
 Route::post('/nowaodpowiedz/{id_posta}', 'OdpowiedziController@nowaodpowiedz')->name('nowaodpowiedz');                   //!!! do zrobienia
 


Route::get('przenoszenieposta/{id}', 'PrzenoszenieController@przeniesClient')->name('przenoszenieposta');



Route::get('kostkomat', 'KostkomatController@index')->name('kostkomat'); ;


Route::get('obrazeniomat', 'ObrazeniomatController@index')->name('obrazeniomat'); ;

Route::get('generatorpostaci', 'GeneratorPostaciController@index')->name('generatorpostaci'); ;



Route::get('czat', 'CzatController@index')->name('czat'); 
Route::post('czat', 'CzatController@create')->name('czat');





Route::get('kalkulator', 'KalkulatorController@index')->name('kalkulator');
Route::get('losowaniedwor', 'LosowanieDworController@index')->name('losowaniedwor');
Route::get('losowanielufy', 'LosowanieLufyController@index')->name('losowanielufy');
Route::get('losowanieoblivion', 'LosowanieOblivionController@index')->name('losowanieoblivion');
Route::get('losowaniepopulacja', 'LosowaniePopulacjaController@index')->name('losowaniepopulacja');
Route::get('losowaniepostac', 'LosowaniePostacController@index')->name('losowaniepostac');
Route::get('losowaniepotwor', 'LosowaniePotworController@index')->name('losowaniepotwor');
Route::get('losowanietechnologie', 'LosowanieTechnologieController@index')->name('losowanietechnologie');

Route::get('okretomat', 'OkretomatController@index')->name('okretomat');







//  Route::post('edytujposta/{id}', 'BibliotekaController@edytujPostacZapisz')->name('edytujpostazapisz');  //!!!! 
//  Route::post('edytujposta/{id}', 'HomeController@edytujPostacZapisz')->name('edytujpostazapisz');  //!!!! 




// 
// Route::get('przeniespostaci/{id}', 'ListaPostaciController@przeniesPostac')->name('usuwaniepostaci');
// Route::get('przeniesgracza/{id}', 'ListaGraczyController@przeniesGracz')->name('usuwaniegracza');