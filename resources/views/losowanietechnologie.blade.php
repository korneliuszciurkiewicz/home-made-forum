


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    


        <script src="//cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>

        <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>


        <link rel="apple-touch-icon" sizes="180x180" href="/favicon.ico">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon.ico">  
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico">
        <link rel="manifest" href="/site.webmanifest">
        <!-- Styles -->
        <style>
        .zaloguj{
    color: chocolate;
}
.wyloguj{
color: chocolate;
}

.nawigacjaNawigacja{
font-weight: bold;

}
.nawigacjaWydarzenia{
    font-weight: bold;

}
.nawigacjaPrzygody{
    font-weight: bold;
 
}
.nawigacjaKarczma{
    font-weight: bold;

.nawigacjaWielkaBiblioteka{
    font-weight: bold;

}
.nawigacjaPostacie{
    font-weight: bold;

}

.tematTytul{
font-size: large;
font-weight: bold;
}
.tematTresc{
    
    font-weight: 600;
}
.graczPlayer{
font-style: italic;
}
.aktywniGracze{
    font-style: italic;

}

.tematData{
    font-style: italic;
}









.container{
            font-family: Brush Script MT, Brush Script Std, cursive; 
        }


        </style>
    </head>
    <body>
     


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">


<div class="container">
 
<div class="row">

        <div  class="col-sm">
            <a href="<?php echo url('/') ?>">
          <img src="obrazy/kompas1.png" style="width: 40%" href="index.html"></div>
            </a>

        <div  class="col-sm">

        <button type="button" class="button"> <a href="<?php echo url('newpost') ?>"> Dodaj nowy post </a> </button>
        </div>

        <div  class="col-sm">
        @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        </div>
 





</div>

</div>


<div class="container">
 
    <div class="row"> 
       
        <div  class="col-sm">
        <div class="nawigacjaNawigacja">  <a href="<?php echo url('/') ?>"> Nawigacja:</a></div>
            <br><br> 
            <div  class="nawigacjaWydarzenia"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('wydarzenia') ?>">  Wydarzenia</a></div><br>
           <div  class="nawigacjaPrzygody">   <img src="dragon.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('przygody') ?>">  Przygody</a> </div><br>
           <div  class="nawigacjaKarczma">    <img src="mermaid.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('karczma') ?>"> Karczma </a></div><br>
           <div  class="nawigacjaWielkaBiblioteka">  <img src="pumpkin-carriage.png" alt="Girl in a jacket" width="30px" height="30px">          <a href="<?php echo url('biblioteka') ?>">   Wielka Biblioteka</a></div><br>
           <div  class="nawigacjaPostacie">   <img src="hat.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listapostaci') ?>">     Postacie</a> </div><br>
           <div  class="nawigacjaPostacie">   <img src="werewolf.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listagraczy') ?>">     Gracze</a> </div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('obrazeniomat') ?>">  Obrazeniomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kostkomat') ?>">  Kostkomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('generatorpostaci') ?>">  Postaciomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kalkulator') ?>">  Kalkulator</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniedwor') ?>"> Losowanie dwór </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanielufy') ?>"> Losowanie lufy </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanieoblivion') ?>">Losowanie atrybutów  </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepopulacja') ?>"> Losowanie populacja</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepostac') ?>"> Losowanie postac</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepotwor') ?>"> Losowanie potwor</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanietechnologie') ?>"> Losowanie  technologie</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('okretomat') ?>"> Okrętomat</a></div><br>

           <br>
        </div>







        <div  class="col-sm">
            <p></p>
            <b> Technologie opanowane przez daną cywilizację:</b>
            <p></p>
            
            <script> 
            var i=0;  //zmienna podstawowa
            while (i<=9) //dziesięć razy powtarza pętlę
            {
                let odkrycie = Math.floor(Math.random() * 79); 
                i++;
                {document.write('<p></p>');}
                if (odkrycie <= 1)
                 {document.write('Rolnictwo');}
               
                 else if (odkrycie <= 2){
                     document.write('Hodowla zwierząt');
                 }
                   
                 else if (odkrycie <= 3){
                     document.write('Łucznictwo ');
                 }  
                 else if (odkrycie <= 4){
                     document.write('Brąz');
                 }  
                 else if (odkrycie <= 5){
                     document.write('Kalendarz');
                 }  
                 else if (odkrycie <= 6){
                     document.write('Murarstwo');
                 }  
                 else if (odkrycie <= 7){
                     document.write('Górnictwo  ');
                 }  
                 else if (odkrycie <= 8){
                     document.write('Garncarstwo');
                 }  
                 else if (odkrycie <= 9){
                     document.write('Żegluga');
                 }  
                 else if (odkrycie <= 10){
                     document.write('Koło');
                 }  
                 else if (odkrycie <=11){
                     document.write('Łowiectwo');
                 }  
                 else if (odkrycie <= 12){
                     document.write('Pisarstwo');
                 }  
                 else if (odkrycie <= 13){
                     document.write('Budownictwo');
                 }  
                 else if (odkrycie <= 14){
                     document.write('Waluta');
                 }  
                 else if (odkrycie <= 15){
                     document.write('Dramat i poezja');
                 }  
                 else if (odkrycie <= 16){
                     document.write('Inżynieria');
                 }  
                 else if (odkrycie <= 17){
                     document.write('Jazda konna');
                 }  
                 else if (odkrycie <= 18){
                     document.write('Żelazo');
                 }  
                 else if (odkrycie <= 19){
                     document.write('Matematyka');
                 }  
                 else if (odkrycie <= 20){
                     document.write('Optyka');
                 }  
                 else if (odkrycie <= 21){
                     document.write('Filozifia');
                 }  
                 else if (odkrycie <= 22){
                     document.write('Etos rycerski');
                 }  
                 else if (odkrycie <= 23){
                     document.write('Służba publiczna');
                 }  
                 else if (odkrycie <= 24){
                     document.write('Kompas');
                 }  
                 else if (odkrycie <= 25){
                     document.write('Edukacja  publiczna');
                 }  
                 else if (odkrycie <= 26){
                     document.write('Gildie');
                 }  
                 else if (odkrycie <= 27){
                     document.write('Maszyneria');
                 }  
                 else if (odkrycie <= 28){
                     document.write('Piece hutnicze');
                 }  
                 else if (odkrycie <= 29){
                     document.write('Fizyka');
                 }  
                 else if (odkrycie <= 30){
                     document.write('Stal');
                 }  
                 else if (odkrycie <= 31){
                     document.write('Teologia');
                 }  
                 else if (odkrycie <= 32){
                     document.write('Akustyka');
                 }
                 else if (odkrycie <= 33){
                     document.write('Architektura');
                 }
                      else if (odkrycie <= 34){
                     document.write('Astronomia');
                 }
            
                 else if (odkrycie <= 35){
                     document.write('Bankowość');
                 }
            
                 else if (odkrycie <= 36){
                     document.write('Chemia');
                 }
            
                 else if (odkrycie <= 37){
                     document.write('Ekonomia');
                 }
            
            
                 else if (odkrycie <= 38){
                     document.write('Proch strzelniczy');
                 }
            
            
                 else if (odkrycie <= 39){
                     document.write('Metalurgia');
                 }
            
            
                 else if (odkrycie <= 40){
                     document.write('Nawigacja');
                 }
            
            
                 else if (odkrycie <= 41){
                     document.write('Drukowana prasa');
                 }
            
            
                 else if (odkrycie <= 42){
                     document.write('Archeologia');
                 }
            
            
                 else if (odkrycie <= 43){
                     document.write('Biologia');
                 }
            
            
                 else if (odkrycie <= 44){
                     document.write('Dynamit');
                 }
            
                 else if (odkrycie <= 45){
                     document.write('Elektryczność');
                 }
            
            
                 else if (odkrycie <= 46){
                     document.write('Nawozy');
                 }
            
            
                 else if (odkrycie <= 47){
                     document.write('Industrializacja');
                 }
            
                 else if (odkrycie <= 48){
                     document.write('Akademie Wojskowe');
                 }
            
                 else if (odkrycie <= 49){
                     document.write('Karabiny');
                 }
            
                 else if (odkrycie <= 50){
                     document.write('Teoria naukowa');
                 }
            
            
                 else if (odkrycie <= 51){
                     document.write('Energia parowa');
                 }
            
                 else if (odkrycie <= 52){
                     document.write('Balistyka');
                 }
            
            
                 else if (odkrycie <= 52){
                     document.write('Silnik spalinowy');
                 }
            
                 else if (odkrycie <= 53){
                     document.write('Elektronika');
                 }
            
                 else if (odkrycie <= 54){
                     document.write('Lotnictwo ');
                 }
            
                 else if (odkrycie <= 55){
                     document.write('Kolej ');
                 }
            
                 else if (odkrycie <= 56){
                     document.write('Plastik');
                 }
            
            
                 else if (odkrycie <= 57){
                     document.write('Radio ');
                 }
            
                 
                 else if (odkrycie <= 58){
                     document.write('Rafinacja');
                 }
            
                 
                 else if (odkrycie <= 59){
                     document.write('Wymiana narządów');
                 }
            
                 
                 else if (odkrycie <= 60){
                     document.write('Teoria atomu');
                 }
            
                 
                 else if (odkrycie <= 61){
                     document.write('Połączone siły zbrojne');
                 }
                 
                 
                 else if (odkrycie <= 62){
                     document.write('Komputery ');
                 }
                 
                 else if (odkrycie <= 63){
                     document.write('Ekologia');
                 }
                 
                 else if (odkrycie <= 64){
                     document.write('Fuzja nuklearna');
                 }
                 
                 else if (odkrycie <= 65){
                     document.write('Antybiotyki ');
                 }
                 
                 else if (odkrycie <= 66){
                     document.write('Radar ');
                 }
            
                 else if (odkrycie <= 67){
                     document.write('Rakiety');
                 }
            
            
                 else if (odkrycie <= 68){
                     document.write('Zaawansowana balistyka ');
                 }
            
            
                 else if (odkrycie <= 69){
                     document.write('Globalizacja');
                 }
            
            
                 else if (odkrycie <= 70){
                     document.write('Lasery');
                 }
            
            
                 else if (odkrycie <= 71){
                     document.write('Taktyka mobilna ');
                 }
            
            
                 else if (odkrycie <= 72){
                     document.write('Nanotechnologia ');
                 }
            
            
                 else if (odkrycie <= 73){
                     document.write('Nuclear fuzja ');
                 }
            
                 
                 else if (odkrycie <= 74){
                     document.write('Fizyka kwantowa');
                 }
            
                 
                 else if (odkrycie <= 75){
                     document.write('Robotyka');
                 }
            
            
                 
                 else if (odkrycie <= 76){
                     document.write('Satelity');
                 }
            
            
                 
                 else if (odkrycie <= 77){
                     document.write('Kamuflaż');
                 }
                 
                 else if (odkrycie <= 78){
                     document.write('Telekomiunikacja');
                 }
                 
                 else if (odkrycie <= 79){
                     document.write('Internet');
                 }
            }
            </script>





<br> <br><br> <br><br> <br>


<b> Doktryny wojskowe i duch armii:</b>
<p></p>

<script> 
var i=0;  //zmienna podstawowa
while (i<=9) //dziesięć razy powtarza pętlę
{
    let odkrycie = Math.floor(Math.random() * 79); 
    i++;
    {document.write('<p></p>');}
    if (odkrycie <= 1)
     {document.write('Tunele wojny - armia doskonale radzi sobie z kopaniem tuneli i podziemnych sieci łacznikowych ');}
   
     else if (odkrycie <= 2){
         document.write('Gigantyczne pola minowe ');
     }
       
     else if (odkrycie <= 3){
         document.write(' Precyzyjna artyleria ');
     }  
     else if (odkrycie <= 4){
         document.write('Zmasowany ostrzał artyleryjski');
     }  
     else if (odkrycie <= 5){
         document.write('Oddziały zaporowe');
     }  
     else if (odkrycie <= 6){
         document.write('Strzelcy wyborowi');
     }  
     else if (odkrycie <= 7){
         document.write(' Snajperzy weterani ');
     }  
     else if (odkrycie <= 8){
         document.write('Snajperzy gwardii');
     }  
     else if (odkrycie <= 9){
         document.write('Gigantyczne działa');
     }  
     else if (odkrycie <= 10){
         document.write('Mobilne punkty dowodzenia');
     }  
     else if (odkrycie <=11){
         document.write('Centralny system dowodzenia');
     }  
     else if (odkrycie <= 12){
         document.write('Zdecentralizowane dowodzenie');
     }  
     else if (odkrycie <= 13){
         document.write('Potężne linie fortyfikacji');
     }  
     else if (odkrycie <= 14){
         document.write('Desanty powietrzne');
     }  
     else if (odkrycie <= 15){
         document.write('20 osobowa drużyna piechoty');
     }  
     else if (odkrycie <= 16){
         document.write('10 osobowa drużyna piechoty');
     }  
     else if (odkrycie <= 17){
         document.write('Asi pancerni');
     }  
     else if (odkrycie <= 18){
         document.write('Nacisk na maskowanie artylerii');
     }  
     else if (odkrycie <= 19){
         document.write('Działa niszczyciele-czołgów');
     }  
     else if (odkrycie <= 20){
         document.write('Spowalnianie wroga');
     }  
     else if (odkrycie <= 21){
         document.write('Elitarna piechota');
     }  
     else if (odkrycie <= 22){
         document.write(' Masowe szkolenie ');
     }  
     else if (odkrycie <= 23){
         document.write(' Kosmiczni marines');
     }  
     else if (odkrycie <= 24){
         document.write(' Lekkie czołgi');
     }  
     else if (odkrycie <= 25){
         document.write(' Średnie czołgi ');
     }  
     else if (odkrycie <= 26){
         document.write(' Ciężkie czołgi');
     }  
     else if (odkrycie <= 27){
         document.write(' Mobilne miny');
     }  
     else if (odkrycie <= 28){
         document.write(' Inżynierowie ');
     }  
     else if (odkrycie <= 29){
         document.write(' Milicja');
     }  
     else if (odkrycie <= 30){
         document.write(' Komisarze polityczni');
     }  
     else if (odkrycie <= 31){
         document.write(' Kapłani bitewni');
     }  
     else if (odkrycie <= 32){
         document.write(' Bersekerzy');
     }
     else if (odkrycie <= 33){
         document.write('Super-ciężka piechota szturmowa');
     }
          else if (odkrycie <= 34){
         document.write('Drużyny motocyklistów');
     }

     else if (odkrycie <= 35){
         document.write(' Pancerne rozpoznanie');
     }

     else if (odkrycie <= 36){
         document.write(' Dywersanci');
     }

     else if (odkrycie <= 37){
         document.write(' Zmasowane zasieki');
     }


     else if (odkrycie <= 38){
         document.write(' Masowy napalm ');
     }


     else if (odkrycie <= 39){
         document.write(' Masowe karabiny maszynowe');
     }


     else if (odkrycie <= 40){
         document.write(' Doskonała logistyka');
     }


     else if (odkrycie <= 41){
         document.write(' Generałowie-bohaterowie ');
     }


     else if (odkrycie <= 42){
         document.write(' Sprofesjonalizowany sztab');
     }


     else if (odkrycie <= 43){
         document.write(' Nacisk na wywiad strategiczny');
     }


     else if (odkrycie <= 44){
         document.write(' Wywiadowcy polowi');
     }

     else if (odkrycie <= 45){
         document.write(' Doskonali lekarze');
     }


     else if (odkrycie <= 46){
         document.write(' Artyleria zdecentralizowana');
     }


     else if (odkrycie <= 47){
         document.write('Altyleria scentralizowana');
     }

     else if (odkrycie <= 48){
         document.write(' Kompanie karne ');
     }

     else if (odkrycie <= 49){
         document.write(' Łowcy czołgów ');
     }

     else if (odkrycie <= 50){
         document.write('Elastyczna obrona ');
     }


     else if (odkrycie <= 51){
         document.write(' Walka nocna jako podstawa');
     }

     else if (odkrycie <= 52){
         document.write('Wojna partyzancka');
     }


     else if (odkrycie <= 52){
         document.write(' Warowne obozy');
     }

     else if (odkrycie <= 53){
         document.write(' Wojna psychologiczna ');
     }

     else if (odkrycie <= 54){
         document.write(' Komanda dziecięce');
     }

     else if (odkrycie <= 55){
         document.write(' Volkszturm ');
     }

     else if (odkrycie <= 56){
         document.write(' Łączność flagowa');
     }


     else if (odkrycie <= 57){
         document.write(' Łączność elektroniczna ');
     }

     
     else if (odkrycie <= 58){
         document.write(' Unikanie walki w miastach');
     }

     
     else if (odkrycie <= 59){
         document.write(' Nacisk na walkę w miastach ');
     }

     
     else if (odkrycie <= 60){
         document.write(' Doskonała logistyka ');
     }

     
     else if (odkrycie <= 61){
         document.write(' Unikanie walki w lasach ');
     }
     
     
     else if (odkrycie <= 62){
         document.write(' Nacisk na walkę w lasach');
     }
     
     else if (odkrycie <= 63){
         document.write(' Taktyka salami');
     }
     
     else if (odkrycie <= 64){
         document.write(' Mentalność: Zwycięstwo albo śmierć ');
     }
     
     else if (odkrycie <= 65){
         document.write('Mentalność: Przełamać i zalać ');
     }
     
     else if (odkrycie <= 66){
         document.write('Mentalność: Nieuchwytność ');
     }

     else if (odkrycie <= 67){
         document.write('Mentalność: Jesteśmy zawodowcami');
     }


     else if (odkrycie <= 68){
         document.write('Mentalność: Pocisk jest tańszy niż życie ');
     }


     else if (odkrycie <= 69){
         document.write('Mentalność: Twierdze! Okopy! Inżynierowie!');
     }


     else if (odkrycie <= 70){
         document.write('Mentalność: Prymat ducha nad materią!');
     }


     else if (odkrycie <= 71){
         document.write(' Mentalność: Informacje to podstawa ');
     }


     else if (odkrycie <= 72){
         document.write('Mentalność: Dywersja to podstawa ');
     }


     else if (odkrycie <= 73){
         document.write(' Mentalność: Szybkość działania ');
     }

     
     else if (odkrycie <= 74){
         document.write('Mentalność: Kult maszyny ');
     }

     
     else if (odkrycie <= 75){
         document.write('Mentalność: Spowolnić, wymęczyć i wyniszczyć');
     }


     
     else if (odkrycie <= 76){
         document.write('Mentalność: Szok, otoczenie i rozbicie ');
     }


     
     else if (odkrycie <= 77){
         document.write('Mentalność: Absolutny profesjonalizm');
     }
     
     else if (odkrycie <= 78){
         document.write('Mentalność: Nacisk na walkę w trudnym terenie ');
     }
     
     else if (odkrycie <= 79){
         document.write('Mentalność: Totalna elastyczność');
     }
}
</script>











              </div>




        

<div class="container">
  
    <div class="row"> 
        
        <div  class="col-sm">
        <a href="<?php echo url('chat') ?>">  Non Clima Chat </a>
        </div>

        <div  class="col-sm">
         
        </div>

        <div  class="col-sm">
            <div class="wyloguj"> <a href="wylogowanie.blade.php"> Wyloguj się </a></div> 
        </div>
    </div>


</div>








    </body>
</html>
