


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>



        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>My Chart.js Chart</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    


        <script src="//cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>

        <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>


        <link rel="apple-touch-icon" sizes="180x180" href="/favicon.ico">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon.ico">  
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico">
        <link rel="manifest" href="/site.webmanifest">
        <!-- Styles -->
        <style>
        .zaloguj{
    color: chocolate;
}
.wyloguj{
color: chocolate;
}

.nawigacjaNawigacja{
font-weight: bold;

}
.nawigacjaWydarzenia{
    font-weight: bold;

}
.nawigacjaPrzygody{
    font-weight: bold;
 
}
.nawigacjaKarczma{
    font-weight: bold;

.nawigacjaWielkaBiblioteka{
    font-weight: bold;

}
.nawigacjaPostacie{
    font-weight: bold;

}

.tematTytul{
font-size: large;
font-weight: bold;
}
.tematTresc{
    
    font-weight: 600;
}
.graczPlayer{
font-style: italic;
}
.aktywniGracze{
    font-style: italic;

}

.tematData{
    font-style: italic;
}









.container{
            font-family: Brush Script MT, Brush Script Std, cursive; 
        }


        </style>
    </head>
    <body>
     


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">


<div class="container">
 
<div class="row">

        <div  class="col-sm">
            <a href="<?php echo url('/') ?>">
          <img src="obrazy/kompas1.png" style="width: 40%" href="index.html"></div>
            </a>

        <div  class="col-sm">

        <button type="button" class="button"> <a href="<?php echo url('newpost') ?>"> Dodaj nowy post </a> </button>
        </div>

        <div  class="col-sm">
        @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        </div>
 





</div>

</div>


<div class="container">
 
    <div class="row"> 
       
        <div  class="col-sm">
        <div class="nawigacjaNawigacja">  <a href="<?php echo url('/') ?>"> Nawigacja:</a></div>
            <br><br> 
            <div  class="nawigacjaWydarzenia"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('wydarzenia') ?>">  Wydarzenia</a></div><br>
           <div  class="nawigacjaPrzygody">   <img src="dragon.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('przygody') ?>">  Przygody</a> </div><br>
           <div  class="nawigacjaKarczma">    <img src="mermaid.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('karczma') ?>"> Karczma </a></div><br>
           <div  class="nawigacjaWielkaBiblioteka">  <img src="pumpkin-carriage.png" alt="Girl in a jacket" width="30px" height="30px">          <a href="<?php echo url('biblioteka') ?>">   Wielka Biblioteka</a></div><br>
           <div  class="nawigacjaPostacie">   <img src="hat.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listapostaci') ?>">     Postacie</a> </div><br>
           <div  class="nawigacjaPostacie">   <img src="werewolf.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listagraczy') ?>">     Gracze</a> </div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('obrazeniomat') ?>">  Obrazeniomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kostkomat') ?>">  Kostkomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('generatorpostaci') ?>">  Postaciomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kalkulator') ?>">  Kalkulator</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniedwor') ?>"> Losowanie dwór </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanielufy') ?>"> Losowanie lufy </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanieoblivion') ?>">Losowanie atrybutów  </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepopulacja') ?>"> Losowanie populacja</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepostac') ?>"> Losowanie postac</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepotwor') ?>"> Losowanie potwor</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanietechnologie') ?>"> Losowanie  technologie</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('okretomat') ?>"> Okrętomat</a></div><br>

           <br>
        </div>







        <div  class="col-sm">  
            <canvas id="myChart"></canvas>
      
    
        <script>
            let random =10000 + (Math.floor(Math.random() * 1001) + 1)
            let random1 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random;
            let random2 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random1;
            let random3 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random2;
            let random4 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random3;
            let random5 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random4;
            let random6 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random5;
            let random7 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random6;
            let random8 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random7;
            let random9 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random8;
            let random10 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random9;
            let random11 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random10;
            let random12 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random11;
            let random13 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random12;
            let random14 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random13;
            let random15 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random14;
            let random16 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random15;
            let random17 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random16;
            let random18 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random17;
            let random19 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random18;
            let random20 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random19;
            let random21 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random20;
            let random22 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random21;
            let random23 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random22;
            let random24 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random23;
            let random25 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random24;
            let random26 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random25;
            let random27 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random26;
            let random28 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random27;
            let random29 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random28;
            let random30 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random29;
            let random31 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random30;
            let random32 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random31;
            let random33 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random32;
            let random34 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random33;
            let random35 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random34;
            let random36 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random35;
            let random37 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random36;
            let random38 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random37;
            let random39 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random38;
            let random40 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random39;
            let random41 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random40;
            let random42 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random41;
            let random43 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random42;
            let random44 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random43;
            let random45 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random44;
            let random46 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random45;
            let random47 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random46;
            let random48 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random47;
            let random49 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random48;
            let random50 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random49;
            let random51 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random50;
            let random52 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random51;
            let random53 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random52;
            let random54 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random53;
            let random55 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random54;
            let random56 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random55;
            let random57 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random56;
            let random58 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random57;
            let random59 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random58;
            let random60 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random59;
            let random61 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random60;
            let random62 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random61;
            let random63 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random62;
            let random64 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random63;
            let random65 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random64;
            let random66 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random65;
            let random67 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random66;
            let random68 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random67;
            let random69 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random68;
            let random70 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random69;
            let random71 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random70;
            let random72 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random71;
            let random73 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random72;
            let random74 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random73;
            let random75 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random74;
            let random76 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random75;
            let random77 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random76;
            let random78 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random77;
            let random79 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random78;
            let random80 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random79;
            let random81 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random80;
            let random82 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random81;
            let random83 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random82;
            let random84 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random83;
            let random85 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random84;
            let random86 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random85;
            let random87 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random86;
            let random88 = (Math.floor(Math.random() * 1000) + 1) + (Math.floor(Math.random() * 1001) - 1001) + random87;
            document.write(random);
        </script>
    
    
    
        <script>
            let myChart = document.getElementById('myChart').getContext('2d');
    
            // Global Options
            Chart.defaults.global.defaultFontFamily = 'Lato';
            Chart.defaults.global.defaultFontSize = 18;
            Chart.defaults.global.defaultFontColor = '#777';
    
            let massPopChart = new Chart(myChart, {
                type: 'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
                data: {
                    labels: ['1000', '1025', '1050', '1075', '1100', '1125', '1150', '1175', '1200', '1225', '1250', '1275', '1300', '1325', '1350', '1375', '1400', '1425', '1450', '1475', '1500', '1525', '1550', '1575', '1600', '1625', '1650', '1675', '1700', '1750', '1775', '1800', '1825', '1850', '1875', '1900', '1925', '1950', '1975', '2000', '2025', '2050', '2075', '2100', '2125', '2150', '2175', '2200', '2225', '2250', '2275', '2300', '2325', '2350', '2375', '2400', '2425', '2450', '2475', '2500', '2525', '2550', '2575', '2600', '2625', '2650', '2675', '2700', '2725', '2750', '2775', '2800', '2825', '2850', '2875', '2900', '2925', '2950', '2975', '3000', '3025', '3050', '3075', '3100', '3125', '3150', '3175', '3200', '3225', '3250'],
                    datasets: [{
                        label: 'Population',
                        data: [
                            random,
                            random1,
                            random2,
                            random3,
                            random4,
                            random5,
                            random6,
                            random7,
                            random8,
                            random9,
                            random10,
                            random11,
                            random12,
                            random13,
                            random14,
                            random15,
                            random16,
                            random17,
                            random18,
                            random19,
                            random20,
                            random21,
                            random22,
                            random23,
                            random24,
                            random25,
                            random26,
                            random27,
                            random28,
                            random29,
                            random30,
                            random31,
                            random32,
                            random33,
                            random34,
                            random35,
                            random36,
                            random37,
                            random38,
                            random39,
                            random40,
                            random41,
                            random42,
                            random43,
                            random44,
                            random45,
                            random46,
                            random47,
                            random48,
                            random49,
                            random50,
                            random51,
                            random52,
                            random53,
                            random54,
                            random55,
                            random56,
                            random57,
                            random58,
                            random59,
                            random60,
                            random61,
                            random62,
                            random63,
                            random64,
                            random65,
                            random66,
                            random67,
                            random68,
                            random69,
                            random70,
                            random71,
                            random72,
                            random73,
                            random74,
                            random75,
                            random76,
                            random77,
                            random78,
                            random79,
                            random80,
                            random81,
                            random82,
                            random83,
                            random84,
                            random85,
                            random86,
                            random87,
                            random88
                        ],
                        //backgroundColor:'green',
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(75, 192, 192, 0.6)',
                            'rgba(153, 102, 255, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            'rgba(255, 99, 132, 0.6)'
                        ],
                        borderWidth: 1,
                        borderColor: '#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Populacja krainy',
                        fontSize: 25
                    },
                    legend: {
                        display: true,
                        position: 'right',
                        labels: {
                            fontColor: '#000'
                        }
                    },
                    layout: {
                        padding: {
                            left: 50,
                            right: 0,
                            bottom: 0,
                            top: 0
                        }
                    },
                    tooltips: {
                        enabled: true
                    }
                }
            });
        </script>
  



        

<div class="container">
  
    <div class="row"> 
        
        <div  class="col-sm">
        <a href="<?php echo url('chat') ?>">  Non Clima Chat </a>
        </div>

        <div  class="col-sm">
         
        </div>

        <div  class="col-sm">
            <div class="wyloguj"> <a href="wylogowanie.blade.php"> Wyloguj się </a></div> 
        </div>
    </div>


</div>








    </body>
</html>
