


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  


        <script src="//cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>

        <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>


        <link rel="apple-touch-icon" sizes="180x180" href="/favicon.ico">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon.ico">  
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico">
        <link rel="manifest" href="/site.webmanifest">
        <!-- Styles -->
        <style>
        .zaloguj{
    color: chocolate;
}
.wyloguj{
color: chocolate;
}

.nawigacjaNawigacja{
font-weight: bold;
width:auto;
background-color: #F7F5FB; 
}
.nawigacjaWydarzenia{
    font-weight: bold;
    width:auto;
    background-color: #F7F5FB; 
}
.nawigacjaPrzygody{
    font-weight: bold;
    width:auto;
    background-color: #F7F5FB; 
}
.nawigacjaKarczma{
    font-weight: bold;
    width:auto;
    background-color: #F7F5FB; 
}
.nawigacjaWielkaBiblioteka{
    font-weight: bold;
    width:auto;
    background-color: #F7F5FB; 
}
.nawigacjaPostacie{
    font-weight: bold;
    width:auto;
    background-color: #F7F5FB; 
}

.tematTytul{
font-size: large;
font-weight: bold;
}
.tematTresc{
    
    font-weight: 600;
}
.graczPlayer{
font-style: italic;
}
.aktywniGracze{
    font-style: italic;

}

.tematData{
    font-style: italic;
}






        
.container{
            font-family: Brush Script MT, Brush Script Std, cursive; 
        }





        </style>
    </head>
    <body>
     


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">


<div class="container">
 
<div class="row">

        <div  class="col-sm">
            <a href="<?php echo url('/') ?>">
          <img src="obrazy/kompas1.png" style="width: 40%" href="index.html"></div>
            </a>

        <div  class="col-sm">

        <button type="button" class="button"> <a href="<?php echo url('nowapostac') ?>">Stwórz nową postać! </a> </button>
        </div>

        <div  class="col-sm">
        @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        </div>
 





</div>

</div>


<div class="container">
 
    <div class="row"> 
       
        <div  class="col-sm">
        <div class="nawigacjaNawigacja">       <a href="<?php echo url('/') ?>"> Nawigacja:</a></div>
            <br><br> 
            <div  class="nawigacjaWydarzenia"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('wydarzenia') ?>">  Wydarzenia</a></div><br>
           <div  class="nawigacjaPrzygody">   <img src="dragon.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('przygody') ?>">  Przygody</a> </div><br>
           <div  class="nawigacjaKarczma">    <img src="mermaid.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('karczma') ?>"> Karczma </a></div><br>
           <div  class="nawigacjaWielkaBiblioteka">  <img src="pumpkin-carriage.png" alt="Girl in a jacket" width="30px" height="30px">          <a href="<?php echo url('biblioteka') ?>">   Wielka Biblioteka</a></div><br>
           <div  class="nawigacjaPostacie">   <img src="hat.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listapostaci') ?>">     Postacie</a> </div><br>
           <div  class="nawigacjaPostacie">   <img src="werewolf.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listagraczy') ?>">     Gracze</a> </div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('obrazeniomat') ?>">  Obrazeniomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kostkomat') ?>">  Kostkomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('generatorpostaci') ?>">  Postaciomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kalkulator') ?>">  Kalkulator</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniedwor') ?>"> Losowanie dwór </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanielufy') ?>"> Losowanie lufy </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanieoblivion') ?>">Losowanie atrybutów  </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepopulacja') ?>"> Losowanie populacja</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepostac') ?>"> Losowanie postac</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepotwor') ?>"> Losowanie potwor</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanietechnologie') ?>"> Losowanie  technologie</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('okretomat') ?>"> Okrętomat</a></div><br>

           <br>
        </div>

        <div  class="col-sm">
    <!-- PLIK listapostaci.blade.php zawierać będzie wyświetlaną listę wszystkich postaci jakie zostały stworzone w grze. Lista ta, będzie wyswietlana w formie identycznej
    jak forma listy graczy. Po wejściu w daną postać na liście postaci zobaczymy: id, avatar, imię postaci, oraz opis.  -->
  

<div class="zdobieniaDiva" style="font-size: 15px">  <a href='/'> </a></div>
Istniejące postacie w grze 


@foreach($kartypostaci as $kartypostaci) 

<div class="zdobieniaDiva" style="font-size: 15px; border-style: solid; text-align:center" > 
<b> ID:   </b> {{$kartypostaci->id}} <br>
<b>Imię:</b>   {{$kartypostaci->postac}} <br>
<b> Opis: </b> {{$kartypostaci->opis}} <br>   
<b>Tajemne:</b> To jest tajemnica, prawda? <!-- {{$kartypostaci->tajemne}}  --> <br>
<b> Avatar:</b>   <img src="{{$kartypostaci->avatar}}" style="width: 500px"> <br>
<b>Rasa: </b>   {{$kartypostaci->rasaPostaci}} <br>
<b>Stworzono: </b>   {{$kartypostaci->created_at}} <br>
<b> Ostatnia modyfikacja:</b>  {{$kartypostaci->updated_at}} <br>
<b>Stworzył:  </b> {{$kartypostaci->id_wlasciciela}} <br>

<a href="{!!route("edytujpostac", ['id' => $kartypostaci->id])!!}">Edytuj</a> <a href="{!!route("usuwaniepostaci", ['id' => $kartypostaci->id])!!}">Usuń </a><button>Przenieś </button> <br><br>

</div>




                <br><br><br>

        @endforeach




 </div>
              
       

        <div  class="col-sm">
            <br>
            <div class="aktywniGracze" > </div>Zarejestrowani gracze: <br>

            @foreach($users as $user) 

<div class="zdobieniaDiva" style="font-size: 15px"> 
           <a href='{{route("getuser", $user->id)}}'> {!!$user->name!!} </a>
         
</div>


        @endforeach 






        </div>


    </div>

</div>


<div class="container">
  
    <div class="row"> 
        
        <div  class="col-sm">
        <a href="<?php echo url('chat') ?>">  Non Clima Chat </a>
        </div>

        <div  class="col-sm">
       
        </div>

        <div  class="col-sm">
            <div class="wyloguj"> <a href="wylogowanie.blade.php"> Wyloguj się </a></div> 
        </div>
    </div>


</div>








    </body>
</html>
