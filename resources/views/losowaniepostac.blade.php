


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <script src="//cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>

        <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>


        <link rel="apple-touch-icon" sizes="180x180" href="/favicon.ico">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon.ico">  
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico">
        <link rel="manifest" href="/site.webmanifest">
        <!-- Styles -->
        <style>
        .zaloguj{
    color: chocolate;
}
.wyloguj{
color: chocolate;
}

.nawigacjaNawigacja{
font-weight: bold;

}
.nawigacjaWydarzenia{
    font-weight: bold;

}
.nawigacjaPrzygody{
    font-weight: bold;
 
}
.nawigacjaKarczma{
    font-weight: bold;

.nawigacjaWielkaBiblioteka{
    font-weight: bold;

}
.nawigacjaPostacie{
    font-weight: bold;

}

.tematTytul{
font-size: large;
font-weight: bold;
}
.tematTresc{
    
    font-weight: 600;
}
.graczPlayer{
font-style: italic;
}
.aktywniGracze{
    font-style: italic;

}

.tematData{
    font-style: italic;
}









.container{
            font-family: Brush Script MT, Brush Script Std, cursive; 
        }


        </style>
    </head>
    <body>
     


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">


<div class="container">
 
<div class="row">

        <div  class="col-sm">
            <a href="<?php echo url('/') ?>">
          <img src="obrazy/kompas1.png" style="width: 40%" href="index.html"></div>
            </a>

        <div  class="col-sm">

        <button type="button" class="button"> <a href="<?php echo url('newpost') ?>"> Dodaj nowy post </a> </button>
        </div>

        <div  class="col-sm">
        @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        </div>
 





</div>

</div>


<div class="container">
 
    <div class="row"> 
       
        <div  class="col-sm">
        <div class="nawigacjaNawigacja">  <a href="<?php echo url('/') ?>"> Nawigacja:</a></div>
            <br><br> 
            <div  class="nawigacjaWydarzenia"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('wydarzenia') ?>">  Wydarzenia</a></div><br>
           <div  class="nawigacjaPrzygody">   <img src="dragon.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('przygody') ?>">  Przygody</a> </div><br>
           <div  class="nawigacjaKarczma">    <img src="mermaid.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('karczma') ?>"> Karczma </a></div><br>
           <div  class="nawigacjaWielkaBiblioteka">  <img src="pumpkin-carriage.png" alt="Girl in a jacket" width="30px" height="30px">          <a href="<?php echo url('biblioteka') ?>">   Wielka Biblioteka</a></div><br>
           <div  class="nawigacjaPostacie">   <img src="hat.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listapostaci') ?>">     Postacie</a> </div><br>
           <div  class="nawigacjaPostacie">   <img src="werewolf.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listagraczy') ?>">     Gracze</a> </div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('obrazeniomat') ?>">  Obrazeniomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kostkomat') ?>">  Kostkomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('generatorpostaci') ?>">  Postaciomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kalkulator') ?>">  Kalkulator</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniedwor') ?>"> Losowanie dwór </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanielufy') ?>"> Losowanie lufy </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanieoblivion') ?>">Losowanie atrybutów  </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepopulacja') ?>"> Losowanie populacja</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepostac') ?>"> Losowanie postac</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepotwor') ?>"> Losowanie potwor</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanietechnologie') ?>"> Losowanie  technologie</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('okretomat') ?>"> Okrętomat</a></div><br>

           <br>
        </div>







        <div  class="col-sm">    
            <b>  Wielkość:</b>
          <script>
              let trait1 = Math.floor(Math.random() * 101); 
                 
                 if (trait1 <= 10)
                  {document.write('Wysoki wzrost');}
                  else if (trait1 <= 20){
                      document.write('Niski wzrost');
                  }
                  else if (trait1 >= 21){
                      document.write('Przeciętny wzrost');
                  }
          </script>
              
              <p></p>
          
          <b> Masa:</b>
              <script>
                     let trait2 = Math.floor(Math.random() * 101); 
                     
                     if (trait2 <= 10)
                  {document.write('Gruby');}
                  else if (trait2 <= 20){
                      document.write('Chudy');
                  }
                  else if (trait2 <= 30){
                      document.write('Wysportowany');
                  }
                  else if (trait2 >= 31){
                      document.write('Przeciętna waga');
                  }
              </script>
          <p></p>
          
          
          <b> Miejsce zamieszkania:</b>
            <script>
           let trait3 = Math.floor(Math.random() * 101); 
           if (trait3 <= 5)
            {document.write('Samotny dom');}
          
            else if (trait3 <= 10){
                document.write('Samotny dom');
            }
          
            else if (trait3 <= 15){
                document.write('Samotny dom');
            }
          
          
            else if (trait3 <= 20){
                document.write('Wioska');
            }
          
          
            else if (trait3 <= 25){
                document.write('Wioska');
            }
          
          
            else if (trait3 <= 30){
                document.write('Wieś');
            }
          
          
            else if (trait3 <= 35){
                document.write('Wieś');
            }
          
          
            else if (trait3 <= 40){
                document.write('Wieś');
            }
          
          
          
            else if (trait3 <= 45){
                document.write('Miasteczko');
            }
          
          
            else if (trait3 <= 50){
                document.write('Miasteczko');
            }
          
          
            else if (trait3 <= 55){
                document.write('Miasteczko');
            }
          
          
            else if (trait3 <= 60){
                document.write('Dwór');
            }
          
          
          
            else if (trait3 <= 65){
                document.write('Dwór');
            }
          
          
            else if (trait3 <= 70){
                document.write('Zamek');
            }
          
          
            else if (trait3 <= 75){
                document.write('Zamek');
            }
          
          
            else if (trait3 <= 80){
                document.write('Miasto');
            }
          
            else if (trait3 <= 85){
                document.write('Miasto');
            }
            
            else if (trait3 <= 90){
                document.write('Metropolia');
            }
            
            else if (trait3 <= 95){
                document.write('Metropolia');
            }
            
            else if (trait3 <= 100){
                document.write('Metropolia');
            }
          
          
          </script>
          
          <p>
          </p>
          
          
          <b> Twoja rodzina: </b>
          <script>
              let trait4 = Math.floor(Math.random() * 101); 
              if (trait4 <= 5)
               {document.write('Sierota');}
             
               else if (trait4 <= 10){
                   document.write('Siostra');
               }
             
               else if (trait4 <= 15){
                   document.write('Matka');
               }
             
             
               else if (trait4 <= 20){
                   document.write('Ojciec');
               }
             
             
               else if (trait4 <= 25){
                   document.write('Siostra i brat');
               }
             
             
               else if (trait4 <= 30){
                   document.write('Dwie siostry');
               }
             
             
               else if (trait4 <= 35){
                   document.write('Siostra i matka');
               }
             
             
               else if (trait4 <= 40){
                   document.write('Siostra i ojciec');
               }
             
             
             
               else if (trait4 <= 45){
                   document.write('Brat i ojciec');
               }
             
             
               else if (trait4 <= 50){
                   document.write('Brat i matka');
               }
             
             
               else if (trait4 <= 55){
                   document.write('Dwóch braci');
               }
             
             
               else if (trait4 <= 60){
                   document.write('Rodzice');
               }
             
             
             
               else if (trait4 <= 65){
                   document.write('Cała rodzina  (do 5 osób)');
               }
             
             
               else if (trait4 <= 70){
                   document.write('Cała rodzina (do 10 osób)');
               }
             
             
               else if (trait4 <= 75){
                   document.write('Cała rodzina (do 15 osób)');
               }
             
             
               else if (trait4 <= 80){
                   document.write('Mały klan (około 40 członków)');
               }
             
               else if (trait4 <= 85){
                   document.write('Duży klan (kilkuset członków)');
               }
               
               else if (trait4 <= 90){
                   document.write('Cała rodzina  (do 5 osób)');
               }
               
               else if (trait4 <= 95){
                   document.write('Cała rodzina (do 10 osób)');
               }
               
               else if (trait4 <= 100){
                   document.write('Cała rodzina  (do 5 osób)');
               }
             
          
             </script>
             
          
          
          <p></p>
          
          <b> Pochodzenie społeczne:</b>
             <script>
              let trait5 = Math.floor(Math.random() * 101); 
              if (trait5 <= 5)
               {document.write('Niewolnik');}
             
               else if (trait5 <= 10){
                   document.write('Niewolnik');
               }
             
               else if (trait5 <= 15){
                   document.write('Niewolnik');
               }
             
             
               else if (trait5 <= 20){
                   document.write('Robotnik');
               }
             
             
               else if (trait5 <= 25){
                   document.write('Robotnik');
               }
             
             
               else if (trait5 <= 30){
                   document.write('Robotnik');
               }
             
             
               else if (trait5 <= 35){
                   document.write('Chłop');
               }
             
             
               else if (trait5 <= 40){
                   document.write('Chłop');
               }
             
             
             
               else if (trait5 <= 45){
                   document.write('Chłop');
               }
             
             
               else if (trait5 <= 50){
                   document.write('Artysta');
               }
             
             
               else if (trait5 <= 55){
                   document.write('Artysta');
               }
             
             
               else if (trait5 <= 60){
                   document.write('Rzemieślnik');
               }
             
             
             
               else if (trait5 <= 65){
                   document.write('Rzemieślnik');
               }
             
             
               else if (trait5 <= 70){
                   document.write('Duchowny');
               }
             
             
               else if (trait5 <= 75){
                   document.write('Duchowny');
               }
             
             
               else if (trait5 <= 80){
                   document.write('Żołnierz');
               }
             
               else if (trait5 <= 85){
                   document.write('Żołnierz');
               }
               
               else if (trait5 <= 90){
                   document.write('Arystokrata');
               }
               
               else if (trait5 <= 95){
                   document.write('Szlachcic');
               }
               
               else if (trait5 <= 100){
                   document.write('Ród królewski');
               }
             
          
             </script>
             
          <p></p>
          
          <b>Religijność:</b>
          <script>
              let trait6 = Math.floor(Math.random() * 101); 
              if (trait6 <= 5)
               {document.write('Heretyk');}
             
               else if (trait6 <= 10){
                   document.write('Heretyk');
               }
             
               else if (trait6 <= 15){
                   document.write('Niechętny');
               }
             
             
               else if (trait6 <= 20){
                   document.write('Niechętny');
               }
             
             
               else if (trait6 <= 25){
                   document.write('Sceptyczny');
               }
             
             
               else if (trait6 <= 30){
                   document.write('Sceptyczny');
               }
             
             
               else if (trait6 <= 35){
                   document.write('Obojętny');
               }
             
             
               else if (trait6 <= 40){
                   document.write('Obojętny');
               }
             
             
             
               else if (trait6 <= 45){
                   document.write('Obojętny');
               }
             
             
               else if (trait6 <= 50){
                   document.write('Wierzący-niepraktykujący');
               }
             
             
               else if (trait6 <= 55){
                   document.write('Wierzący-niepraktykujący');
               }
             
             
               else if (trait6 <= 60){
                   document.write('Wierzący-niepraktykujący');
               }
             
             
             
               else if (trait6 <= 65){
                   document.write('Przeciętnie pobożny');
               }
             
             
               else if (trait6 <= 70){
                   document.write('Przeciętnie pobożny');
               }
             
             
               else if (trait6 <= 75){
                   document.write('Przeciętnie pobożny');
               }
             
             
               else if (trait6 <= 80){
                   document.write('Gorliwy wyznawca');
               }
             
               else if (trait6 <= 85){
                   document.write('Gorliwy wyznawca');
               }
               
               else if (trait6 <= 90){
                   document.write('Gorliwy wyznawca');
               }
               
               else if (trait6 <= 95){
                   document.write('Fanatyk');
               }
               
               else if (trait6 <= 100){
                   document.write('Fanatyk');
               }
             
          
             </script>
          
           
          
           
          
           
           
           
           
          
          <p></p>
          <b>Ulubiony przedmiot: </b>
          <script>
              let trait7 = Math.floor(Math.random() * 101); 
              if (trait7 <= 5)
               {document.write('Miecz');}
             
               else if (trait7 <= 10){
                   document.write('Czaszkę');
               }
             
               else if (trait7 <= 15){
                   document.write('Hełm');
               }
             
             
               else if (trait7 <= 20){
                   document.write('Kufel do piwa ');
               }
             
             
               else if (trait7 <= 25){
                   document.write('Tarczę');
               }
             
             
               else if (trait7 <= 30){
                   document.write('Kluczyk');
               }
             
             
               else if (trait7 <= 35){
                   document.write('Naszyjnik');
               }
             
             
               else if (trait7 <= 40){
                   document.write('Pióro');
               }
             
             
             
               else if (trait7 <= 45){
                   document.write('Buty');
               }
             
             
               else if (trait7 <= 50){
                   document.write('Pierścień');
               }
             
             
               else if (trait7 <= 55){
                   document.write('Książka');
               }
             
             
               else if (trait7 <= 60){
                   document.write('Relikwia');
               }
             
             
             
               else if (trait7 <= 65){
                   document.write('Kapelusz');
               }
             
             
               else if (trait7 <= 70){
                   document.write('Topór');
               }
             
             
               else if (trait7 <= 75){
                   document.write('Szata');
               }
             
             
               else if (trait7 <= 80){
                   document.write('Zbroja');
               }
             
               else if (trait7 <= 85){
                   document.write('Łuk');
               }
               
               else if (trait7 <= 90){
                   document.write('Pierścień');
               }
               
               else if (trait7 <= 95){
                   document.write('Piesek');
               }
               
               else if (trait7 <= 100){
                   document.write('Kotek');
               }
             
          
             </script>
          
          
          <p></p>
          
          <b>Służy:</b>
          <script>
              let trait8 = Math.floor(Math.random() * 101); 
              if (trait8 <= 5)
               {document.write('Królowi');}
             
               else if (trait8 <= 10){
                   document.write('Królowi');
               }
             
               else if (trait8 <= 15){
                   document.write('Królowi');
               }
             
             
               else if (trait8 <= 20){
                   document.write('Świątyni');
               }
             
             
               else if (trait8 <= 25){
                   document.write('Świątyni');
               }
             
             
               else if (trait8 <= 30){
                   document.write('Gangowi');
               }
             
             
               else if (trait8 <= 35){
                   document.write('Gangowi');
               }
             
             
               else if (trait8 <= 40){
                   document.write('Gildii');
               }
             
             
             
               else if (trait8 <= 45){
                   document.write('Gildii');
               }
             
             
               else if (trait8 <= 50){
                   document.write('Gildii');
               }
             
             
               else if (trait8 <= 55){
                   document.write('Bractwu wojowników');
               }
             
             
               else if (trait8 <= 60){
                   document.write('Bractwu wojowników');
               }
             
             
             
               else if (trait8 <= 65){
                   document.write('Wolny strzeec');
               }
             
             
               else if (trait8 <= 70){
                   document.write('Wolny strzeec');
               }
             
             
               else if (trait8 <= 75){
                   document.write('Wolny strzeec');
               }
             
             
               else if (trait8 <= 80){
                   document.write('Swojej rodzinie');
               }
             
               else if (trait8 <= 85){
                   document.write('Swojej rodzinie');
               }
               
               else if (trait8 <= 90){
                   document.write('Swojej rodzinie');
               }
               
               else if (trait8 <= 95){
                   document.write('Swojej rodzinie');
               }
               
               else if (trait8 <= 100){
                   document.write('Stowarzyszeniu uczonych');
               }
             
          
             </script>
          
          
          
          
          <p></p>
          <b>
          Charakterystyczne cechy:</b>
          <p></p>
          
          <script>
              let cecha = Math.floor(Math.random() * 245); 
              if (cecha <= 2)
               {document.write('Czuły');}
             
               else if (cecha <= 4){
                   document.write('Wyniosły');
               }
               else if (cecha <= 6){
                   document.write('Figlarny');
               }
               else if (cecha <= 8){
                   document.write('Hałaśliwy');
               }
               else if (cecha <= 10){
                   document.write('Sumienny');
               }
               else if (cecha <= 12){
                   document.write('Wybredny');
               }
               else if (cecha <= 14){
                   document.write('Przemyśliwujący');
               }
               else if (cecha <= 16){
                   document.write('Samowolny');
               }
               else if (cecha <= 18){
                   document.write('Leniwy');
               }
               else if (cecha <= 20){
                   document.write('Ciekawski');
               }
               else if (cecha <= 22){
                   document.write('Zapatrzony w siebie');
               }
               else if (cecha <= 24){
                   document.write('Nieśmiały');
               }
               else if (cecha <= 26){
                   document.write('Przygnębiony');
               }
               else if (cecha <= 28){
                   document.write('Pijak');
               }
               else if (cecha <= 30){
                   document.write('Niedołężny');
               }
               else if (cecha <= 32){
                   document.write('Zestresowany');
               }
               else if (cecha <= 34){
                   document.write('Człowiek z blizną');
               }
               else if (cecha<= 36){
                   document.write('Opętany');
               }
               else if (cecha<= 38){
                   document.write('Wariat');
               }
               else if (cecha<= 40){
                   document.write('Okaleczony');
               }
               else if (cecha<= 42){
                   document.write('Długowieczny');
               }
               else if (cecha<= 44){
                   document.write('Pulchny');
               }
               else if (cecha<= 46){
                   document.write('Kościsty');
               }
               else if (cecha<= 48){
                   document.write('Zniekształcony');
               }
               else if (cecha<= 50){
                   document.write('Jednooki');
               }
               else if (cecha<= 52){
                   document.write('Jednoręczny');
               }
               else if (cecha<= 54){
                   document.write('Jednonogi');
               }
               else if (cecha<= 56){
                   document.write('Drań');
               }
               else if (cecha<= 58){
                   document.write('Bękart');
               }
               else if (cecha<= 60){
                   document.write('Bliźniak');
               }
               else if (cecha<= 62){
                   document.write('Dziecko konkubiny');
               }
               else if (cecha<= 64){
                   document.write('Urodzony w purpurze');
               }
               else if (cecha<= 66){
                   document.write('Atrakcyjny');
               }
               else if (cecha<= 68){
                   document.write('Geniusz');
               }
               else if (cecha<= 70){
                   document.write('Bystry');
               }
               else if (cecha<= 72){
                   document.write('Silny');
               }
               else if (cecha<= 74){
                   document.write('Wielka stopa');
               }
               else if (cecha<= 76){
                   document.write('Karzełek');
               }
               else if (cecha<= 78){
                   document.write('Zajęcza warga');
               }
               else if (cecha<= 80){
                   document.write('Garbus');
               }
               else if (cecha<= 82){
                   document.write('Imbecyl');
               }
               else if (cecha<= 84){
                   document.write('Owoc kazirodztwa');
               }
               else if (cecha<= 86){
                   document.write('Seplenienie');
               }
               else if (cecha<= 88){
                   document.write('Powolny');
               }
               else if (cecha<= 90){
                   document.write('Jąkała');
               }
               else if (cecha<= 92){
                   document.write('Brzydki');
               }
               else if (cecha<= 94){
                   document.write('Słaby');
               }
               else if (cecha<= 96){
                   document.write('Ogromny');
               }
               else if (cecha<= 98){
                   document.write('Leworęczny');
               }
               else if (cecha<= 100){
                   document.write('Krzepki');
               }
          
               else if (cecha<= 102){
                   document.write('Silny');
               }   
               
               else if (cecha<= 104){
                   document.write('Wątły');
               }
          
               
               else if (cecha<= 106){
                   document.write('Sprytny');
               }
          
               
               else if (cecha<= 108){
                   document.write('Nudny');
               }
          
               
               else if (cecha<= 110){
                   document.write('Wypielęgnowany');
               }
          
               
               else if (cecha<= 112){
                   document.write('Nieokrzesany');
               }
          
               
               else if (cecha<= 114){
                   document.write('Administrator');
               }
          
               
               else if (cecha<= 116){
                   document.write('Architekt');
               }
          
               
               else if (cecha<= 118){
                   document.write('Pojedynkowicz');
               }
          
               
               else if (cecha<= 120){
                   document.write('Umysł stratega');
               }
          
               
               else if (cecha<= 122){
                   document.write('Ogrodnik');
               }
          
               
               else if (cecha<= 124){
                   document.write('Hedonista');
               }
          
               
               else if (cecha<= 126){
                   document.write('Łowca');
               }
          
               
               else if (cecha<= 128){
                   document.write('Miłośnik tortur');
               }
          
               
               else if (cecha<= 130){
                   document.write('Intrygant');
               }
          
               
               else if (cecha<= 132){
                   document.write('Uwodziciel');
               }
          
               
               else if (cecha<= 134){
                   document.write('Mistyk');
               }
          
               
               else if (cecha<= 136){
                   document.write('Uczony');
               }
          
               
               else if (cecha<= 138){
                   document.write('Towarzyski');
               }
          
               else if (cecha<= 140){
                   document.write('Strateg');
               }
          
               
               else if (cecha<= 142){
                   document.write('Teolog');
               }
          
               
               else if (cecha<= 144){
                   document.write('Sokolnik');
               }
          
               
               else if (cecha<= 146){
                   document.write('Żyjacy w celibacie ');
               }
          
               
               else if (cecha<= 148){
                   document.write('Poeta');
               }
          
               
               else if (cecha<= 150){
                   document.write('Czysty');
               }
          
               
               else if (cecha<= 152){
                   document.write('Umiarkowany');
               }
          
               
               else if (cecha<= 154){
                   document.write('Charytatywny');
               }
          
               
               else if (cecha<= 156){
                   document.write('Pracowity');
               }
          
               
               else if (cecha<= 158){
                   document.write('Cierpliwy');
               }
          
               
               else if (cecha<= 160){
                   document.write('Uprzejmy');
               }
          
               
               else if (cecha<= 162){
                   document.write('Pokorny');
               }
          
               
               else if (cecha<= 164){
                   document.write('Zmysłowy');
               }
          
               
               else if (cecha<= 166){
                   document.write('Żarłoczny');
               }
          
               
               else if (cecha<= 168){
                   document.write('Chciwy');
               }
          
          
               else if (cecha<= 170){
                   document.write('Leniwy');
               }
          
               else if (cecha<= 172){
                   document.write('Gniewny');
               }
          
               else if (cecha<= 174){
                   document.write('Dumny');
               }
          
               else if (cecha<= 176){
                   document.write('Zazdrosny');
               }
          
               else if (cecha<= 178){
                   document.write('Ambitny');
               }
               else if (cecha<= 180){
                   document.write('Arbitralny');
               }
          
               else if (cecha<= 182){
                   document.write('Tchórz');
               }
          
          
               else if (cecha<= 184){
                   document.write('Odważny');
               }
               else if (cecha<= 186){
                   document.write('Zawartość');
               }
          
          
               else if (cecha<= 188){
                   document.write('Okrutny');
               }
          
          
               else if (cecha<= 190){
                   document.write('Cyniczny');
               }
          
          
               else if (cecha<= 192){
                   document.write('Podstępny');
               }
          
          
               else if (cecha<= 194){
                   document.write('Erudyta');
               }
          
          
               else if (cecha<= 196){
                   document.write('Towarzyski');
               }
          
               else if (cecha<= 198){
                   document.write('Szczery');
               }
          
          
               else if (cecha<= 200){
                   document.write('Sprawiedliwy');
               }
          
          
               else if (cecha<= 202){
                   document.write('Paranoik');
               }
              
               else if (cecha<= 204){
                   document.write('Paranoik');
               } 
                 else if (cecha<= 204){
                   document.write('Nieśmiały');
               } 
                 
                 else if (cecha<= 206){
                   document.write('Uparty');
               }  
               
                else if (cecha<= 208){
                   document.write('Ufny');
               } 
                
                 else if (cecha<= 210){
                   document.write('Gorliwy');
               } 
               
                 else if (cecha<= 212){
                   document.write('Poszukiwacz przygód');
               }
                
                  else if (cecha<= 214){
                   document.write('Prawowity imperator');
               }
                
                  else if (cecha<= 216){
                   document.write('Oślepiony');
               }
                  
                  else if (cecha<= 218){
                   document.write('Kanibal');
               }  
                
                else if (cecha<= 220){
                   document.write('Kot');
               } 
                
                 else if (cecha<= 222){
                   document.write('Eunuch');
               }  
                
                else if (cecha<= 224){
                   document.write('Ekskomunikowany');
               }
                 
          
          else if (cecha<= 226){
                   document.write('Gladiator');
               } 
               
                 else if (cecha<= 228){
                   document.write('Herezjarcha');
               }
                
                  else if (cecha<= 230){
                   document.write('Homoseksualny');
               }  
               
                else if (cecha<= 232){
                   document.write('Koń');
               }
          
          
               else if (cecha<= 234){
                   document.write('W konspiracji');
               } 
               
                 else if (cecha <= 236){
                   document.write('Choroba weneryczna');
               }
                
                  else if (cecha <= 238){
                   document.write('W podrózy');
               }  
               
                else if (cecha <= 240){
                   document.write('Chłopski wódz');
               }
          
               else if (cecha <= 242){
                   document.write('Reinkarnacja');
               } 
               
                 else if (cecha >= 243){
                   document.write('Gwardzista');
               }
                
             </script> 
          
          
          <p></p>
          
          
          
          <script>
                  let cecha1 = Math.floor(Math.random() * 245); 
                  if (cecha1 <= 2)
                   {document.write('Czuły');}
                 
                   else if (cecha1 <= 4){
                       document.write('Wyniosły');
                   }
                   else if (cecha1 <= 6){
                       document.write('Figlarny');
                   }
                   else if (cecha1 <= 8){
                       document.write('Hałaśliwy');
                   }
                   else if (cecha1 <= 10){
                       document.write('Sumienny');
                   }
                   else if (cecha1 <= 12){
                       document.write('Wybredny');
                   }
                   else if (cecha1 <= 14){
                       document.write('Przemyśliwujący');
                   }
                   else if (cecha1 <= 16){
                       document.write('Samowolny');
                   }
                   else if (cecha1 <= 18){
                       document.write('Leniwy');
                   }
                   else if (cecha1 <= 20){
                       document.write('Ciekawski');
                   }
                   else if (cecha1 <= 22){
                       document.write('Zapatrzony w siebie');
                   }
                   else if (cecha1 <= 24){
                       document.write('Nieśmiały');
                   }
                   else if (cecha1 <= 26){
                       document.write('Przygnębiony');
                   }
                   else if (cecha1 <= 28){
                       document.write('Pijak');
                   }
                   else if (cecha1 <= 30){
                       document.write('Niedołężny');
                   }
                   else if (cecha1 <= 32){
                       document.write('Zestresowany');
                   }
                   else if (cecha1 <= 34){
                       document.write('Człowiek z blizną');
                   }
                   else if (cecha1 <= 36){
                       document.write('Opętany');
                   }
                   else if (cecha1 <= 38){
                       document.write('Wariat');
                   }
                   else if (cecha1 <= 40){
                       document.write('Okaleczony');
                   }
                   else if (cecha1 <= 42){
                       document.write('Długowieczny');
                   }
                   else if (cecha1 <= 44){
                       document.write('Pulchny');
                   }
                   else if (cecha1 <= 46){
                       document.write('Kościsty');
                   }
                   else if (cecha1 <= 48){
                       document.write('Zniekształcony');
                   }
                   else if (cecha1 <= 50){
                       document.write('Jednooki');
                   }
                   else if (cecha1 <= 52){
                       document.write('Jednoręczny');
                   }
                   else if (cecha1 <= 54){
                       document.write('Jednonogi');
                   }
                   else if (cecha1 <= 56){
                       document.write('Drań');
                   }
                   else if (cecha1 <= 58){
                       document.write('Bękart');
                   }
                   else if (cecha1 <= 60){
                       document.write('Bliźniak');
                   }
                   else if (cecha1 <= 62){
                       document.write('Dziecko konkubiny');
                   }
                   else if (cecha1 <= 64){
                       document.write('Urodzony w purpurze');
                   }
                   else if (cecha1 <= 66){
                       document.write('Atrakcyjny');
                   }
                   else if (cecha1 <= 68){
                       document.write('Geniusz');
                   }
                   else if (cecha1 <= 70){
                       document.write('Bystry');
                   }
                   else if (cecha1 <= 72){
                       document.write('Silny');
                   }
                   else if (cecha1 <= 74){
                       document.write('Wielka stopa');
                   }
                   else if (cecha1 <= 76){
                       document.write('Karzełek');
                   }
                   else if (cecha1 <= 78){
                       document.write('Zajęcza warga');
                   }
                   else if (cecha1 <= 80){
                       document.write('Garbus');
                   }
                   else if (cecha1 <= 82){
                       document.write('Imbecyl');
                   }
                   else if (cecha1 <= 84){
                       document.write('Owoc kazirodztwa');
                   }
                   else if (cecha1 <= 86){
                       document.write('Seplenienie');
                   }
                   else if (cecha1 <= 88){
                       document.write('Powolny');
                   }
                   else if (cecha1 <= 90){
                       document.write('Jąkała');
                   }
                   else if (cecha1 <= 92){
                       document.write('Brzydki');
                   }
                   else if (cecha1 <= 94){
                       document.write('Słaby');
                   }
                   else if (cecha1 <= 96){
                       document.write('Ogromny');
                   }
                   else if (cecha1 <= 98){
                       document.write('Leworęczny');
                   }
                   else if (cecha1 <= 100){
                       document.write('Krzepki');
                   }
              
                   else if (cecha1 <= 102){
                       document.write('Silny');
                   }   
                   
                   else if (cecha1 <= 104){
                       document.write('Wątły');
                   }
              
                   
                   else if (cecha1 <= 106){
                       document.write('Sprytny');
                   }
              
                   
                   else if (cecha1 <= 108){
                       document.write('Nudny');
                   }
              
                   
                   else if (cecha1 <= 110){
                       document.write('Wypielęgnowany');
                   }
              
                   
                   else if (cecha1 <= 112){
                       document.write('Nieokrzesany');
                   }
              
                   
                   else if (cecha1 <= 114){
                       document.write('Administrator');
                   }
              
                   
                   else if (cecha1 <= 116){
                       document.write('Architekt');
                   }
              
                   
                   else if (cecha1 <= 118){
                       document.write('Pojedynkowicz');
                   }
              
                   
                   else if (cecha1 <= 120){
                       document.write('Umysł stratega');
                   }
              
                   
                   else if (cecha1 <= 122){
                       document.write('Ogrodnik');
                   }
              
                   
                   else if (cecha1 <= 124){
                       document.write('Hedonista');
                   }
              
                   
                   else if (cecha1 <= 126){
                       document.write('Łowca');
                   }
              
                   
                   else if (cecha1 <= 128){
                       document.write('Miłośnik tortur');
                   }
              
                   
                   else if (cecha1 <= 130){
                       document.write('Intrygant');
                   }
              
                   
                   else if (cecha1 <= 132){
                       document.write('Uwodziciel');
                   }
              
                   
                   else if (cecha1 <= 134){
                       document.write('Mistyk');
                   }
              
                   
                   else if (cecha1 <= 136){
                       document.write('Uczony');
                   }
              
                   
                   else if (cecha1 <= 138){
                       document.write('Towarzyski');
                   }
              
                   else if (cecha1 <= 140){
                       document.write('Strateg');
                   }
              
                   
                   else if (cecha1 <= 142){
                       document.write('Teolog');
                   }
              
                   
                   else if (cecha1 <= 144){
                       document.write('Sokolnik');
                   }
              
                   
                   else if (cecha1 <= 146){
                       document.write('Żyjacy w celibacie ');
                   }
              
                   
                   else if (cecha1 <= 148){
                       document.write('Poeta');
                   }
              
                   
                   else if (cecha1 <= 150){
                       document.write('Czysty');
                   }
              
                   
                   else if (cecha1 <= 152){
                       document.write('Umiarkowany');
                   }
              
                   
                   else if (cecha1 <= 154){
                       document.write('Charytatywny');
                   }
              
                   
                   else if (cecha1 <= 156){
                       document.write('Pracowity');
                   }
              
                   
                   else if (cecha1 <= 158){
                       document.write('Cierpliwy');
                   }
              
                   
                   else if (cecha1 <= 160){
                       document.write('Uprzejmy');
                   }
              
                   
                   else if (cecha1 <= 162){
                       document.write('Pokorny');
                   }
              
                   
                   else if (cecha1 <= 164){
                       document.write('Zmysłowy');
                   }
              
                   
                   else if (cecha1 <= 166){
                       document.write('Żarłoczny');
                   }
              
                   
                   else if (cecha1 <= 168){
                       document.write('Chciwy');
                   }
              
              
                   else if (cecha1 <= 170){
                       document.write('Leniwy');
                   }
              
                   else if (cecha1 <= 172){
                       document.write('Gniewny');
                   }
              
                   else if (cecha1 <= 174){
                       document.write('Dumny');
                   }
              
                   else if (cecha1 <= 176){
                       document.write('Zazdrosny');
                   }
              
                   else if (cecha1 <= 178){
                       document.write('Ambitny');
                   }
                   else if (cecha1 <= 180){
                       document.write('Arbitralny');
                   }
              
                   else if (cecha1 <= 182){
                       document.write('Tchórz');
                   }
              
              
                   else if (cecha1 <= 184){
                       document.write('Odważny');
                   }
                   else if (cecha1 <= 186){
                       document.write('Zawartość');
                   }
              
              
                   else if (cecha1 <= 188){
                       document.write('Okrutny');
                   }
              
              
                   else if (cecha1 <= 190){
                       document.write('Cyniczny');
                   }
              
              
                   else if (cecha1 <= 192){
                       document.write('Podstępny');
                   }
              
              
                   else if (cecha1 <= 194){
                       document.write('Erudyta');
                   }
              
              
                   else if (cecha1 <= 196){
                       document.write('Towarzyski');
                   }
              
                   else if (cecha1 <= 198){
                       document.write('Szczery');
                   }
              
              
                   else if (cecha1 <= 200){
                       document.write('Sprawiedliwy');
                   }
              
              
                   else if (cecha1 <= 202){
                       document.write('Paranoik');
                   }
                  
                   else if (cecha1 <= 204){
                       document.write('Paranoik');
                   } 
                     else if (cecha1 <= 204){
                       document.write('Nieśmiały');
                   } 
                     
                     else if (cecha1 <= 206){
                       document.write('Uparty');
                   }  
                   
                    else if (cecha1 <= 208){
                       document.write('Ufny');
                   } 
                    
                     else if (cecha1 <= 210){
                       document.write('Gorliwy');
                   } 
                   
                     else if (cecha1 <= 212){
                       document.write('Poszukiwacz przygód');
                   }
                    
                      else if (cecha1 <= 214){
                       document.write('Prawowity imperator');
                   }
                    
                      else if (cecha1 <= 216){
                       document.write('Oślepiony');
                   }
                      
                      else if (cecha1 <= 218){
                       document.write('Kanibal');
                   }  
                    
                    else if (cecha1 <= 220){
                       document.write('Kot');
                   } 
                    
                     else if (cecha1 <= 222){
                       document.write('Eunuch');
                   }  
                    
                    else if (cecha1 <= 224){
                       document.write('Ekskomunikowany');
                   }
                     
              
              else if (cecha1 <= 226){
                       document.write('Gladiator');
                   } 
                   
                     else if (cecha1 <= 228){
                       document.write('Herezjarcha');
                   }
                    
                      else if (cecha1 <= 230){
                       document.write('Homoseksualny');
                   }  
                   
                    else if (cecha1 <= 232){
                       document.write('Koń');
                   }
              
              
                   else if (cecha1 <= 234){
                       document.write('W konspiracji');
                   } 
                   
                     else if (cecha1 <= 236){
                       document.write('Choroba weneryczna');
                   }
                    
                      else if (cecha1 <= 238){
                       document.write('W podrózy');
                   }  
                   
                    else if (cecha1 <= 240){
                       document.write('Chłopski wódz');
                   }
              
                   else if (cecha1 <= 242){
                       document.write('Reinkarnacja');
                   } 
                   
                     else if (cecha1 >= 243){
                       document.write('Gwardzista');
                   }
                    
                 </script> 
          
          <p></p>
          <b>Wierzenia:</b>
          
          <p></p>
          <script>
              let religia = Math.floor(Math.random() * 63); 
              if (religia <= 1)
               {document.write('Kult Przodków');}
             
               else if (religia <= 2){
                   document.write('Pustynne tradycje');
               }
               else if (religia <= 3){
                   document.write('Matka ziemia');
               }
          
               else if (religia <= 4){
                   document.write('Uzdrawianie wiarą');
               }
          
               else if (religia <= 5){
                   document.write('Rytuały płodności');
               }
          
               else if (religia <= 6){
                   document.write('Bóg Rzemiosła');
               }
               else if (religia <= 7){
                   document.write('Bóg Otwartego Nieba');
               }
               else if (religia <= 8){
                   document.write('Bóg Morza');
               }
               else if (religia <= 9){
                   document.write('Bóg Wojny');
               }
               else if (religia <= 10){
                   document.write('Bogini Radości');
               }
               else if (religia <= 11){
                   document.write('Bogini Miłości');
               }
               else if (religia <= 12){
                   document.write('Bogini Opiekuńcza');
               }
               else if (religia <= 13){
                   document.write('Bogini Łowów');
               }
               else if (religia <= 14){
                   document.write('Bóg-Imperator');
               }
               else if (religia <= 15){
                   document.write('Boski Posłaniec');
               }
               else if (religia <= 16){
                   document.write('Boskie Pomniki');
               }
               else if (religia <= 17){
                   document.write('Jedność z Naturą');
               }
               else if (religia <= 18){
                   document.write('Tradycja ustna');
               }
               else if (religia <= 19){
                   document.write('Kult figur');
               }
               else if (religia <= 20){
                   document.write('Świątynne miasta');
               }
               else if (religia <= 21){
                   document.write('Święte Drogi');
               }
               else if (religia <= 22){
                   document.write('Święte Wody');
               }
               else if (religia <= 23){
                   document.write('Kamienne Kręgi');
               }
               else if (religia <= 24){
                   document.write('Bóg Słońca');
               }
               else if (religia <= 25){
                   document.write('Boskie Łzy');
               }
               else if (religia <= 26){
                   document.write('Ceremonie pogrzebowe');
               }
               else if (religia <= 27){
                   document.write('Majątki kościelne');
               }
               else if (religia <= 28){
                   document.write('Ryty inicjacyjne');
               }
               else if (religia <= 29){
                   document.write('Ekumenizm');
               }
               else if (religia <= 30){
                   document.write('Prymat Arcykapłana');
               }
               else if (religia <= 31){
                   document.write('Przekują miecze na lemiesze ');
               }
               else if (religia <= 32){
                   document.write('Pielgrzymstwo');
               }
               else if (religia <= 33){
                   document.write('Dziesięcina');
               }
               else if (religia <= 34){
                   document.write('Światowa wiara');
               }
               else if (religia <= 35){
                   document.write('Dialektyka');
               }
               else if (religia <= 36){
                   document.write('Idea dobra');
               }
               else if (religia <= 37){
                   document.write('Teoria idei');
               }
               else if (religia <= 38){
                   document.write('Dusza nieśmiertelna');
               }
               else if (religia <= 39){
                   document.write('Wędrówka duszy');
               }
               else if (religia <= 40){
                   document.write('Moralność panów i moralność niewolników');
               }
               else if (religia <= 41){
                   document.write('Wola mocy');
               }
               else if (religia <= 42){
                   document.write('Afirmacja i umiłowanie losu');
               }
               else if (religia <= 43){
                   document.write('Wieczny powrót');
               }
               else if (religia <= 44){
                   document.write('Nadczłowiek');
               }
               else if (religia <= 45){
                   document.write('Dekadencja');
               }
               else if (religia <= 46){
                   document.write('Rewolucja kulturalna');
               }
               else if (religia <= 47){
                   document.write('Antyintelektualizm');
               }
               else if (religia <= 49){
                   document.write('Purytanizm');
               }
               else if (religia <= 50){
                   document.write('Antyindywidualizm');
               }
               else if (religia <= 51){
                   document.write('Woluntaryzm');
               }
               else if (religia <= 52){
                   document.write('Wielki Skok');
               }
               else if (religia <= 53){
                   document.write('Kanibalizm');
               }
               else if (religia <= 54){
                   document.write('Podział kastowy');
               }
               else if (religia <= 55){
                   document.write('Reinkarnacja');
               }
               else if (religia <= 56){
                   document.write('Terroryzm religijny');
               }
               else if (religia <= 57){
                   document.write('Umysł Roju');
               }
               else if (religia <= 58){
                   document.write('Anarchizm');
               }
               else if (religia <= 59){
                   document.write('Rytualne pijaństwo');
               }
               else if (religia <= 60){
                   document.write('Rytualna narkomania');
               }
               else if (religia <= 61){
                   document.write('Kult pieniądza');
               }
               else if (religia <= 62){
                   document.write('Władza kobiet');
               }
               else if (religia <= 63){
                   document.write('Miasta schronienia');
               }
          
               </script>
          
          <p></p>
          
          
          
          <script>
              let religia1 = Math.floor(Math.random() * 63); 
              if (religia1 <= 1)
               {document.write('Kult Przodków');}
             
               else if (religia1 <= 2){
                   document.write('Pustynne tradycje');
               }
               else if (religia1 <= 3){
                   document.write('Matka ziemia');
               }
          
               else if (religia1 <= 4){
                   document.write('Uzdrawianie wiarą');
               }
          
               else if (religia1 <= 5){
                   document.write('Rytuały płodności');
               }
          
               else if (religia1 <= 6){
                   document.write('Bóg Rzemiosła');
               }
               else if (religia1 <= 7){
                   document.write('Bóg Otwartego Nieba');
               }
               else if (religia1 <= 8){
                   document.write('Bóg Morza');
               }
               else if (religia1 <= 9){
                   document.write('Bóg Wojny');
               }
               else if (religia1 <= 10){
                   document.write('Bogini Radości');
               }
               else if (religia1 <= 11){
                   document.write('Bogini Miłości');
               }
               else if (religia1 <= 12){
                   document.write('Bogini Opiekuńcza');
               }
               else if (religia1 <= 13){
                   document.write('Bogini Łowów');
               }
               else if (religia1 <= 14){
                   document.write('Bóg-Imperator');
               }
               else if (religia1 <= 15){
                   document.write('Boski Posłaniec');
               }
               else if (religia1 <= 16){
                   document.write('Boskie Pomniki');
               }
               else if (religia1 <= 17){
                   document.write('Jedność z Naturą');
               }
               else if (religia1 <= 18){
                   document.write('Tradycja ustna');
               }
               else if (religia1 <= 19){
                   document.write('Kult figur');
               }
               else if (religia1 <= 20){
                   document.write('Świątynne miasta');
               }
               else if (religia1 <= 21){
                   document.write('Święte Drogi');
               }
               else if (religia1 <= 22){
                   document.write('Święte Wody');
               }
               else if (religia1 <= 23){
                   document.write('Kamienne Kręgi');
               }
               else if (religia1 <= 24){
                   document.write('Bóg Słońca');
               }
               else if (religia1 <= 25){
                   document.write('Boskie Łzy');
               }
               else if (religia1 <= 26){
                   document.write('Ceremonie pogrzebowe');
               }
               else if (religia1 <= 27){
                   document.write('Majątki kościelne');
               }
               else if (religia1 <= 28){
                   document.write('Ryty inicjacyjne');
               }
               else if (religia1 <= 29){
                   document.write('Ekumenizm');
               }
               else if (religia1 <= 30){
                   document.write('Prymat Arcykapłana');
               }
               else if (religia1 <= 31){
                   document.write('Przekują miecze na lemiesze ');
               }
               else if (religia1 <= 32){
                   document.write('Pielgrzymstwo');
               }
               else if (religia1 <= 33){
                   document.write('Dziesięcina');
               }
               else if (religia1 <= 34){
                   document.write('Światowa wiara');
               }
               else if (religia1 <= 35){
                   document.write('Dialektyka');
               }
               else if (religia1 <= 36){
                   document.write('Idea dobra');
               }
               else if (religia1 <= 37){
                   document.write('Teoria idei');
               }
               else if (religia1 <= 38){
                   document.write('Dusza nieśmiertelna');
               }
               else if (religia1 <= 39){
                   document.write('Wędrówka duszy');
               }
               else if (religia1 <= 40){
                   document.write('Moralność panów i moralność niewolników');
               }
               else if (religia1 <= 41){
                   document.write('Wola mocy');
               }
               else if (religia1 <= 42){
                   document.write('Afirmacja i umiłowanie losu');
               }
               else if (religia1 <= 43){
                   document.write('Wieczny powrót');
               }
               else if (religia1 <= 44){
                   document.write('Nadczłowiek');
               }
               else if (religia1 <= 45){
                   document.write('Dekadencja');
               }
               else if (religia1 <= 46){
                   document.write('Rewolucja kulturalna');
               }
               else if (religia1 <= 47){
                   document.write('Antyintelektualizm');
               }
               else if (religia1 <= 49){
                   document.write('Purytanizm');
               }
               else if (religia1 <= 50){
                   document.write('Antyindywidualizm');
               }
               else if (religia1 <= 51){
                   document.write('Woluntaryzm');
               }
               else if (religia1 <= 52){
                   document.write('Wielki Skok');
               }
               else if (religia1 <= 53){
                   document.write('Kanibalizm');
               }
               else if (religia1 <= 54){
                   document.write('Podział kastowy');
               }
               else if (religia1 <= 55){
                   document.write('Reinkarnacja');
               }
               else if (religia1 <= 56){
                   document.write('Terroryzm religijny');
               }
               else if (religia1 <= 57){
                   document.write('Umysł Roju');
               }
               else if (religia1 <= 58){
                   document.write('Anarchizm');
               }
               else if (religia1 <= 59){
                   document.write('Rytualne pijaństwo');
               }
               else if (religia1 <= 60){
                   document.write('Rytualna narkomania');
               }
               else if (religia1 <= 61){
                   document.write('Kult pieniądza');
               }
               else if (religia1 <= 62){
                   document.write('Władza kobiet');
               }
               else if (religia1 <= 63){
                   document.write('Miasta schronienia');
               }
          
               </script>
          
          
          
          
          <p></p>
          
          
          
          
          <script>
              let religia2 = Math.floor(Math.random() * 63); 
              if (religia2 <= 1)
               {document.write('Kult Przodków');}
             
               else if (religia2 <= 2){
                   document.write('Pustynne tradycje');
               }
               else if (religia2 <= 3){
                   document.write('Matka ziemia');
               }
          
               else if (religia2 <= 4){
                   document.write('Uzdrawianie wiarą');
               }
          
               else if (religia2 <= 5){
                   document.write('Rytuały płodności');
               }
          
               else if (religia2 <= 6){
                   document.write('Bóg Rzemiosła');
               }
               else if (religia2 <= 7){
                   document.write('Bóg Otwartego Nieba');
               }
               else if (religia2 <= 8){
                   document.write('Bóg Morza');
               }
               else if (religia2 <= 9){
                   document.write('Bóg Wojny');
               }
               else if (religia2 <= 10){
                   document.write('Bogini Radości');
               }
               else if (religia2 <= 11){
                   document.write('Bogini Miłości');
               }
               else if (religia2 <= 12){
                   document.write('Bogini Opiekuńcza');
               }
               else if (religia2 <= 13){
                   document.write('Bogini Łowów');
               }
               else if (religia2 <= 14){
                   document.write('Bóg-Imperator');
               }
               else if (religia2 <= 15){
                   document.write('Boski Posłaniec');
               }
               else if (religia2 <= 16){
                   document.write('Boskie Pomniki');
               }
               else if (religia2 <= 17){
                   document.write('Jedność z Naturą');
               }
               else if (religia2 <= 18){
                   document.write('Tradycja ustna');
               }
               else if (religia2 <= 19){
                   document.write('Kult figur');
               }
               else if (religia2 <= 20){
                   document.write('Świątynne miasta');
               }
               else if (religia2 <= 21){
                   document.write('Święte Drogi');
               }
               else if (religia2 <= 22){
                   document.write('Święte Wody');
               }
               else if (religia2 <= 23){
                   document.write('Kamienne Kręgi');
               }
               else if (religia2 <= 24){
                   document.write('Bóg Słońca');
               }
               else if (religia2 <= 25){
                   document.write('Boskie Łzy');
               }
               else if (religia2 <= 26){
                   document.write('Ceremonie pogrzebowe');
               }
               else if (religia2 <= 27){
                   document.write('Majątki kościelne');
               }
               else if (religia2 <= 28){
                   document.write('Ryty inicjacyjne');
               }
               else if (religia2 <= 29){
                   document.write('Ekumenizm');
               }
               else if (religia2 <= 30){
                   document.write('Prymat Arcykapłana');
               }
               else if (religia2 <= 31){
                   document.write('Przekują miecze na lemiesze ');
               }
               else if (religia2 <= 32){
                   document.write('Pielgrzymstwo');
               }
               else if (religia2 <= 33){
                   document.write('Dziesięcina');
               }
               else if (religia2 <= 34){
                   document.write('Światowa wiara');
               }
               else if (religia2 <= 35){
                   document.write('Dialektyka');
               }
               else if (religia2 <= 36){
                   document.write('Idea dobra');
               }
               else if (religia2 <= 37){
                   document.write('Teoria idei');
               }
               else if (religia2 <= 38){
                   document.write('Dusza nieśmiertelna');
               }
               else if (religia2 <= 39){
                   document.write('Wędrówka duszy');
               }
               else if (religia2 <= 40){
                   document.write('Moralność panów i moralność niewolników');
               }
               else if (religia2 <= 41){
                   document.write('Wola mocy');
               }
               else if (religia2 <= 42){
                   document.write('Afirmacja i umiłowanie losu');
               }
               else if (religia2 <= 43){
                   document.write('Wieczny powrót');
               }
               else if (religia2 <= 44){
                   document.write('Nadczłowiek');
               }
               else if (religia2 <= 45){
                   document.write('Dekadencja');
               }
               else if (religia2 <= 46){
                   document.write('Rewolucja kulturalna');
               }
               else if (religia2 <= 47){
                   document.write('Antyintelektualizm');
               }
               else if (religia2 <= 49){
                   document.write('Purytanizm');
               }
               else if (religia2 <= 50){
                   document.write('Antyindywidualizm');
               }
               else if (religia2 <= 51){
                   document.write('Woluntaryzm');
               }
               else if (religia2 <= 52){
                   document.write('Wielki Skok');
               }
               else if (religia2 <= 53){
                   document.write('Kanibalizm');
               }
               else if (religia2 <= 54){
                   document.write('Podział kastowy');
               }
               else if (religia2 <= 55){
                   document.write('Reinkarnacja');
               }
               else if (religia2 <= 56){
                   document.write('Terroryzm religijny');
               }
               else if (religia2 <= 57){
                   document.write('Umysł Roju');
               }
               else if (religia2 <= 58){
                   document.write('Anarchizm');
               }
               else if (religia2 <= 59){
                   document.write('Rytualne pijaństwo');
               }
               else if (religia2 <= 60){
                   document.write('Rytualna narkomania');
               }
               else if (religia2 <= 61){
                   document.write('Kult pieniądza');
               }
               else if (religia2 <= 62){
                   document.write('Władza kobiet');
               }
               else if (religia2 <= 63){
                   document.write('Miasta schronienia');
               }
          
               </script>
          
          <p></p>
          
          
          <script>
              let religia3 = Math.floor(Math.random() * 63); 
              if (religia3 <= 1)
               {document.write('Kult Przodków');}
             
               else if (religia3 <= 2){
                   document.write('Pustynne tradycje');
               }
               else if (religia3 <= 3){
                   document.write('Matka ziemia');
               }
          
               else if (religia3 <= 4){
                   document.write('Uzdrawianie wiarą');
               }
          
               else if (religia3 <= 5){
                   document.write('Rytuały płodności');
               }
          
               else if (religia3 <= 6){
                   document.write('Bóg Rzemiosła');
               }
               else if (religia3 <= 7){
                   document.write('Bóg Otwartego Nieba');
               }
               else if (religia3 <= 8){
                   document.write('Bóg Morza');
               }
               else if (religia3 <= 9){
                   document.write('Bóg Wojny');
               }
               else if (religia3 <= 10){
                   document.write('Bogini Radości');
               }
               else if (religia3 <= 11){
                   document.write('Bogini Miłości');
               }
               else if (religia3 <= 12){
                   document.write('Bogini Opiekuńcza');
               }
               else if (religia3 <= 13){
                   document.write('Bogini Łowów');
               }
               else if (religia3 <= 14){
                   document.write('Bóg-Imperator');
               }
               else if (religia3 <= 15){
                   document.write('Boski Posłaniec');
               }
               else if (religia3 <= 16){
                   document.write('Boskie Pomniki');
               }
               else if (religia3 <= 17){
                   document.write('Jedność z Naturą');
               }
               else if (religia3 <= 18){
                   document.write('Tradycja ustna');
               }
               else if (religia3 <= 19){
                   document.write('Kult figur');
               }
               else if (religia3 <= 20){
                   document.write('Świątynne miasta');
               }
               else if (religia3 <= 21){
                   document.write('Święte Drogi');
               }
               else if (religia3 <= 22){
                   document.write('Święte Wody');
               }
               else if (religia3 <= 23){
                   document.write('Kamienne Kręgi');
               }
               else if (religia3 <= 24){
                   document.write('Bóg Słońca');
               }
               else if (religia3 <= 25){
                   document.write('Boskie Łzy');
               }
               else if (religia3 <= 26){
                   document.write('Ceremonie pogrzebowe');
               }
               else if (religia3 <= 27){
                   document.write('Majątki kościelne');
               }
               else if (religia3 <= 28){
                   document.write('Ryty inicjacyjne');
               }
               else if (religia3 <= 29){
                   document.write('Ekumenizm');
               }
               else if (religia3 <= 30){
                   document.write('Prymat Arcykapłana');
               }
               else if (religia3 <= 31){
                   document.write('Przekują miecze na lemiesze ');
               }
               else if (religia3 <= 32){
                   document.write('Pielgrzymstwo');
               }
               else if (religia3 <= 33){
                   document.write('Dziesięcina');
               }
               else if (religia3 <= 34){
                   document.write('Światowa wiara');
               }
               else if (religia3 <= 35){
                   document.write('Dialektyka');
               }
               else if (religia3 <= 36){
                   document.write('Idea dobra');
               }
               else if (religia3 <= 37){
                   document.write('Teoria idei');
               }
               else if (religia3 <= 38){
                   document.write('Dusza nieśmiertelna');
               }
               else if (religia3 <= 39){
                   document.write('Wędrówka duszy');
               }
               else if (religia3 <= 40){
                   document.write('Moralność panów i moralność niewolników');
               }
               else if (religia3 <= 41){
                   document.write('Wola mocy');
               }
               else if (religia3 <= 42){
                   document.write('Afirmacja i umiłowanie losu');
               }
               else if (religia3 <= 43){
                   document.write('Wieczny powrót');
               }
               else if (religia3 <= 44){
                   document.write('Nadczłowiek');
               }
               else if (religia3 <= 45){
                   document.write('Dekadencja');
               }
               else if (religia3 <= 46){
                   document.write('Rewolucja kulturalna');
               }
               else if (religia3 <= 47){
                   document.write('Antyintelektualizm');
               }
               else if (religia3 <= 49){
                   document.write('Purytanizm');
               }
               else if (religia3 <= 50){
                   document.write('Antyindywidualizm');
               }
               else if (religia3 <= 51){
                   document.write('Woluntaryzm');
               }
               else if (religia3 <= 52){
                   document.write('Wielki Skok');
               }
               else if (religia3 <= 53){
                   document.write('Kanibalizm');
               }
               else if (religia3 <= 54){
                   document.write('Podział kastowy');
               }
               else if (religia <= 55){
                   document.write('Reinkarnacja');
               }
               else if (religia3 <= 56){
                   document.write('Terroryzm religijny');
               }
               else if (religia3 <= 57){
                   document.write('Umysł Roju');
               }
               else if (religia3 <= 58){
                   document.write('Anarchizm');
               }
               else if (religia3 <= 59){
                   document.write('Rytualne pijaństwo');
               }
               else if (religia3 <= 60){
                   document.write('Rytualna narkomania');
               }
               else if (religia3 <= 61){
                   document.write('Kult pieniądza');
               }
               else if (religia3 <= 62){
                   document.write('Władza kobiet');
               }
               else if (religia3 <= 63){
                   document.write('Miasta schronienia');
               }
          
               </script>
          
          <p>
          </p>
          
          <P></P>
          
          
          
          
          
          
          </div>




        

<div class="container">
  
    <div class="row"> 
        
        <div  class="col-sm">
        <a href="<?php echo url('chat') ?>">  Non Clima Chat </a>
        </div>

        <div  class="col-sm">
         
        </div>

        <div  class="col-sm">
            <div class="wyloguj"> <a href="wylogowanie.blade.php"> Wyloguj się </a></div> 
        </div>
    </div>


</div>








    </body>
</html>
