


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    


        <script src="//cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>

        <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>


        <link rel="apple-touch-icon" sizes="180x180" href="/favicon.ico">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon.ico">  
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico">
        <link rel="manifest" href="/site.webmanifest">
        <!-- Styles -->
        <style>
        .zaloguj{
    color: chocolate;
}
.wyloguj{
color: chocolate;
}

.nawigacjaNawigacja{
font-weight: bold;

}
.nawigacjaWydarzenia{
    font-weight: bold;

}
.nawigacjaPrzygody{
    font-weight: bold;
 
}
.nawigacjaKarczma{
    font-weight: bold;

.nawigacjaWielkaBiblioteka{
    font-weight: bold;

}
.nawigacjaPostacie{
    font-weight: bold;

}

.tematTytul{
font-size: large;
font-weight: bold;
}
.tematTresc{
    
    font-weight: 600;
}
.graczPlayer{
font-style: italic;
}
.aktywniGracze{
    font-style: italic;

}

.tematData{
    font-style: italic;
}









.container{
            font-family: Brush Script MT, Brush Script Std, cursive; 
        }


        </style>
    </head>
    <body>
     


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">


<div class="container">
 
<div class="row">

        <div  class="col-sm">
            <a href="<?php echo url('/') ?>">
          <img src="obrazy/kompas1.png" style="width: 40%" href="index.html"></div>
            </a>

        <div  class="col-sm">

        <button type="button" class="button"> <a href="<?php echo url('newpost') ?>"> Dodaj nowy post </a> </button>
        </div>

        <div  class="col-sm">
        @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        </div>
 





</div>

</div>


<div class="container">
 
    <div class="row"> 
       
        <div  class="col-sm">
        <div class="nawigacjaNawigacja">  <a href="<?php echo url('/') ?>"> Nawigacja:</a></div>
            <br><br> 
            <div  class="nawigacjaWydarzenia"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('wydarzenia') ?>">  Wydarzenia</a></div><br>
           <div  class="nawigacjaPrzygody">   <img src="dragon.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('przygody') ?>">  Przygody</a> </div><br>
           <div  class="nawigacjaKarczma">    <img src="mermaid.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('karczma') ?>"> Karczma </a></div><br>
           <div  class="nawigacjaWielkaBiblioteka">  <img src="pumpkin-carriage.png" alt="Girl in a jacket" width="30px" height="30px">          <a href="<?php echo url('biblioteka') ?>">   Wielka Biblioteka</a></div><br>
           <div  class="nawigacjaPostacie">   <img src="hat.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listapostaci') ?>">     Postacie</a> </div><br>
           <div  class="nawigacjaPostacie">   <img src="werewolf.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listagraczy') ?>">     Gracze</a> </div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('obrazeniomat') ?>">  Obrazeniomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kostkomat') ?>">  Kostkomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('generatorpostaci') ?>">  Postaciomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kalkulator') ?>">  Kalkulator</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniedwor') ?>"> Losowanie dwór </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanielufy') ?>"> Losowanie lufy </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanieoblivion') ?>">Losowanie atrybutów  </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepopulacja') ?>"> Losowanie populacja</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepostac') ?>"> Losowanie postac</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepotwor') ?>"> Losowanie potwor</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanietechnologie') ?>"> Losowanie  technologie</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('okretomat') ?>"> Okrętomat</a></div><br>

           <br>
        </div>








        <div  class="col-sm">

<form>
Wprowadź parametry okrętu
<br> <br><br> <br>

Długość w metrach:<input type="text" id="dlugosc"> <br> <br>


Ilość największych dział: <input type="text" id="iloscnajwiekszychdzial"> <br> <br>

Ilość średnich dział: <input type="text" id="iloscsrednichdzial"> <br> <br>

Ilość dział pomocniczych: <input type="text" id="iloscmalychdzial"> <br> <br>

Kaliber największego działa:<input type="text" id="najwiekszykaliber"> <br> <br>

Kaliber średniego działa:<input type="text" id="srednikaliber"><br> <br>

Kaliber dział pomocniczych: <input type="text" id="malykaliber"><br> <br>

Maksymalna szybkośc w km: <input type="text" id="predkoscmaksymalna"> <br> <br>

Grubość pancerza podstawowego w cm: <input type="text" id="pancerz"> <br> <br>



<br><button onclick="wykujOkret()">Zatwierdź okret</button> <br>
<br><br><br>

</form>




            <script>
             function wykujOkret(){
                 
                
                    let dlugosc = document.getElementById("dlugosc").value;
                    let najwiekszykaliber = document.getElementById("najwiekszykaliber").value;
                    let srednikaliber = document.getElementById("srednikaliber").value;
                    let malykaliber = document.getElementById("malykaliber").value;
                    let predkoscmaksymalna = document.getElementById("predkoscmaksymalna").value;
                    let iloscnajwiekszychdzial = document.getElementById("iloscnajwiekszychdzial").value;
                    let iloscsrednichdzial = document.getElementById("iloscsrednichdzial").value;
                    let iloscmalychdzial = document.getElementById("iloscmalychdzial").value;
                   
                    let zasiegnajwiekszychdzial = najwiekszykaliber *110;;
                    let zasiegsrednichdzial = srednikaliber *110;; 
                    let zasiegmalychdzial = malykaliber *110;
                    let pancerz = document.getElementById("pancerz").value;;
                    let waga = pancerz*dlugosc; 
                    let wagadzial = (iloscnajwiekszychdzial*najwiekszykaliber)+(iloscsrednichdzial*srednikaliber)+(iloscmalychdzial*malykaliber);
                    let cena = (pancerz+dlugosc+predkoscmaksymalna+wagadzial)/100;


                    document.write('Cena okrętu:'+cena+ ' $'+'<br><br>'); 
                    document.write('Długość okrętu:'+dlugosc+ ' m'+'<br><br>');
                    document.write('Podstawowy pancerz okrętu:'+pancerz+ ' cm'+'<br><br>');
                    document.write('Waga:'+waga+ ' ton'+'<br><br>');
                    document.write('Kaliber największych dział:'+najwiekszykaliber+ ' mm'+'<br><br>');
                    document.write('Kaliber średnich dział:'+srednikaliber+ ' mm'+'<br><br>');
                    document.write('Kaliber najmniejszych dział:'+malykaliber+ ' mm'+'<br><br>');
                    document.write('Prędkość maksymalna:'+predkoscmaksymalna+ ' km/h'+'<br><br>');
                    document.write('Ilośc dział o największym kalibrze:'+iloscnajwiekszychdzial+ ' dzial'+'<br><br>');
                    document.write('Ilość dział o średnim kalibrze:'+iloscsrednichdzial+ ' dzial'+'<br><br>');
                    document.write('Ilość dział o najmniejszym kalibrze:'+iloscmalychdzial+ ' dzial'+'<br><br>');
                    document.write('Zasięg największych dział:'+zasiegnajwiekszychdzial+ ' m'+'<br><br>');
                    document.write('Zasięg srednich dział:'+zasiegsrednichdzial+ ' m'+'<br><br>');
                    document.write('Zasięg malych dział::'+zasiegmalychdzial+ ' m'+'<br><br>');


                  
                        
             }
     </script>









    <div class="glowny">










    Tutaj bedzie generator armat.
    <p>
    </p>
    


 


<br>














      </div>




        

<div class="container">
  
    <div class="row"> 
        
        <div  class="col-sm">
        <a href="<?php echo url('chat') ?>">  Non Clima Chat </a>
        </div>

        <div  class="col-sm">
         
        </div>

        <div  class="col-sm">
            <div class="wyloguj"> <a href="wylogowanie.blade.php"> Wyloguj się </a></div> 
        </div>
    </div>


</div>








    </body>
</html>
