


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   


        <script src="//cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>

        <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>


        <link rel="apple-touch-icon" sizes="180x180" href="/favicon.ico">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon.ico">  
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico">
        <link rel="manifest" href="/site.webmanifest">
        <!-- Styles -->
        <style>
        .zaloguj{
    color: chocolate;
}
.wyloguj{
color: chocolate;
}

.nawigacjaNawigacja{
font-weight: bold;
width:auto;
background-color: #F7F5FB; 
}
.nawigacjaWydarzenia{
    font-weight: bold;
    width:auto;
    background-color: #F7F5FB; 
}
.nawigacjaPrzygody{
    font-weight: bold;
    width:auto;
    background-color: #F7F5FB; 
}
.nawigacjaKarczma{
    font-weight: bold;
    width:auto;
    background-color: #F7F5FB; 
}
.nawigacjaWielkaBiblioteka{
    font-weight: bold;
    width:auto;
    background-color: #F7F5FB; 
}
.nawigacjaPostacie{
    font-weight: bold;
    width:auto;
    background-color: #F7F5FB; 
}

.tematTytul{
font-size: large;
font-weight: bold;
}
.tematTresc{
    
    font-weight: 600;
}
.graczPlayer{
font-style: italic;
}
.aktywniGracze{
    font-style: italic;

}

.tematData{
    font-style: italic;
}








        

.container{
            font-family: Brush Script MT, Brush Script Std, cursive; 
        }







        </style>
    </head>
    <body>
     


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">


<div class="container">
 
<div class="row">

        <div  class="col-sm">
            <a href="<?php echo url('/') ?>">
          <img src="obrazy/kompas1.png" style="width: 40%" href="index.html"></div>
            </a>

        <div  class="col-sm">

        <button type="button" class="button"> <a href="<?php echo url('newpost') ?>"> Dodaj nowy post </a> </button>
        </div>

        <div  class="col-sm">
        @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        </div>
 





</div>

</div>


<div class="container">
 
    <div class="row"> 
       
        <div  class="col-sm">
        <div class="nawigacjaNawigacja">  <a href="<?php echo url('/') ?>"> Nawigacja:</a></div>
            <br><br> 
            <div  class="nawigacjaWydarzenia"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('wydarzenia') ?>">  Wydarzenia</a></div><br>
           <div  class="nawigacjaPrzygody">   <img src="dragon.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('przygody') ?>">  Przygody</a> </div><br>
           <div  class="nawigacjaKarczma">    <img src="mermaid.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('karczma') ?>"> Karczma </a></div><br>
           <div  class="nawigacjaWielkaBiblioteka">  <img src="pumpkin-carriage.png" alt="Girl in a jacket" width="30px" height="30px">          <a href="<?php echo url('biblioteka') ?>">   Wielka Biblioteka</a></div><br>
           <div  class="nawigacjaPostacie">   <img src="hat.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listapostaci') ?>">     Postacie</a> </div><br>
           <div  class="nawigacjaPostacie">   <img src="werewolf.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listagraczy') ?>">     Gracze</a> </div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('obrazeniomat') ?>">  Obrazeniomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kostkomat') ?>">  Kostkomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('generatorpostaci') ?>">  Postaciomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kalkulator') ?>">  Kalkulator</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniedwor') ?>"> Losowanie dwór </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanielufy') ?>"> Losowanie lufy </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanieoblivion') ?>">Losowanie atrybutów  </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepopulacja') ?>"> Losowanie populacja</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepostac') ?>"> Losowanie postac</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepotwor') ?>"> Losowanie potwor</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanietechnologie') ?>"> Losowanie  technologie</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('okretomat') ?>"> Okrętomat</a></div><br>

           <br>
        </div>

        <div  class="col-sm">
    
          
          <p>Cechy </p>

          <button onclick="document.getElementById('demo2').innerHTML = kosccechy(1,10)">Losuj</button>
          
          <p id="demo2"></p>
          
          <script>
          function kosccechy(min, max) {
           var cecha = Math.floor(Math.random() * 100) + 1;
        
            if (cecha < 5) {
              return document.getElementById("demo2").innerHTML = "Bardzo silny ";
            }




            if (cecha < 7) {
              return document.getElementById("demo2").innerHTML = "Bardzo szybki";
            }




              if (cecha < 11) {
              return document.getElementById("demo2").innerHTML = "Błyskotliwość";
            }




            if (cecha < 14) {
              return document.getElementById("demo2").innerHTML = "Bystry wzrok";
            }



            if (cecha < 16) {
              return document.getElementById("demo2").innerHTML = "Charyzmatyczny";
            }





            if (cecha < 18) {
              return document.getElementById("demo2").innerHTML = "Czuły słuch";
            }




            if (cecha < 20) {
              return document.getElementById("demo2").innerHTML = "Geniusz arytmetyczny";
            }






            if (cecha < 23) {
              return document.getElementById("demo2").innerHTML = "Krzepki";
            }




            if (cecha < 28) {
              return document.getElementById("demo2").innerHTML = "Naśladowca";
            }




            if (cecha < 33) {
              return document.getElementById("demo2").innerHTML = "Niezwykle odporny";
            }




            if (cecha < 38) {
              return document.getElementById("demo2").innerHTML = "Oburęczność";
            }





              if (cecha < 40) {
              return document.getElementById("demo2").innerHTML = "Odporność na choroby";
            }



            if (cecha < 50) {
              return document.getElementById("demo2").innerHTML = "Odporność na magię";
            }





            if (cecha < 55) {
              return document.getElementById("demo2").innerHTML = "Odporność na trucizny";
            }




            if (cecha < 60) {
              return document.getElementById("demo2").innerHTML = "Odporność psychiczna";
            }





            if (cecha < 65) {
              return document.getElementById("demo2").innerHTML = " Opanowanie";
            }




            if (cecha < 70) {
              return document.getElementById("demo2").innerHTML = "Strzelec wyborowy";
            }





            if (cecha < 75) {
              return document.getElementById("demo2").innerHTML = "Szczęście";
            }





            if (cecha < 80) {
              return document.getElementById("demo2").innerHTML = "Szósty zmysł";
            }





            if (cecha < 85) {
              return document.getElementById("demo2").innerHTML = "Szybki refleks";
            }





            if (cecha < 90) {
              return document.getElementById("demo2").innerHTML = "Twardziel";
            }


            if (cecha < 95) {
              return document.getElementById("demo2").innerHTML = "Urodzony wojownik";
            }





            if (cecha < 100) {
              return document.getElementById("demo2").innerHTML = "Widzenie w ciemnościach";
            }


            return cecha;
          }


</script>


<br><br><br>





             <p>Profesja </p>

             <button onclick="document.getElementById('demo1').innerHTML = koscprofesji(1,10)">Losuj</button>
             
             <p id="demo1"></p>
             <script>
             function koscprofesji(min, max) {
             
                 var profesja = Math.floor(Math.random() * 100) + 1;
             




                 if (profesja < 2) {
                 return document.getElementById("demo1").innerHTML = "Mechanik";
               }






                 if (profesja < 4) {
                 return document.getElementById("demo1").innerHTML = "Chłop";
               }




               if (profesja < 6) {
                 return document.getElementById("demo1").innerHTML = "Ciura Obozowa";
               }




               if (profesja < 8) {
                 return document.getElementById("demo1").innerHTML = "Cyrkowiec";
               }




               if (profesja < 10) {
                 return document.getElementById("demo1").innerHTML = "Cyrulik";
               }




               if (profesja < 12) {
                 return document.getElementById("demo1").innerHTML = "Fanatyk";
               }




               if (profesja < 14) {
                 return document.getElementById("demo1").innerHTML = "Flisak";
               }





               if (profesja < 16) {
                 return document.getElementById("demo1").innerHTML = "Giermek";
               }




               if (profesja < 18) {
                 return document.getElementById("demo1").innerHTML = "Gladiator";
               }





                 if (profesja < 20) {
                 return document.getElementById("demo1").innerHTML = "Górnik";
               }

                if (profesja < 22) {
                return document.getElementById("demo1").innerHTML = "Guślarz";
              }



if (profesja < 24) {
                 return document.getElementById("demo1").innerHTML = "Hiena Cmentarna";
               }



                 if (profesja < 26) {
                 return document.getElementById("demo1").innerHTML = "Kanciarz";
               }


               if (profesja < 28) {
                 return document.getElementById("demo1").innerHTML = "Kozak kislevski";
               }



               if (profesja < 30) {
                 return document.getElementById("demo1").innerHTML = "Leśnik";
               }


               if (profesja < 32) {
                 return document.getElementById("demo1").innerHTML = "Łowca";
               }



               if (profesja < 34) {
                 return document.getElementById("demo1").innerHTML = "Łowca nagród";
               }



               if (profesja < 36) {
                 return document.getElementById("demo1").innerHTML = "Mieszczanin";
               }



               if (profesja < 38) {
                 return document.getElementById("demo1").innerHTML = "Mytnik";
               }



               if (profesja < 40) {
                 return document.getElementById("demo1").innerHTML = "Najemnik";
               }


               if (profesja < 42) {
                 return document.getElementById("demo1").innerHTML = "Ochotnik";
               }

               if (profesja < 44) {
                 return document.getElementById("demo1").innerHTML = "Ochroniarz";
               }




               if (profesja < 46) {
                 return document.getElementById("demo1").innerHTML = "Oprych";
               }



               if (profesja < 48) {
                 return document.getElementById("demo1").innerHTML = "Paź";
               }



               if (profesja < 50) {
                 return document.getElementById("demo1").innerHTML = "Podżegacz";
               }




               if (profesja < 52) {
                 return document.getElementById("demo1").innerHTML = "Porywacz Zwłok";
               }




               if (profesja < 54) {
                 return document.getElementById("demo1").innerHTML = "Posłaniec";
               }




               if (profesja < 56) {
                 return document.getElementById("demo1").innerHTML = "Przemytnik";
               }




               if (profesja < 58) {
                 return document.getElementById("demo1").innerHTML = "Przepatrywacz";
               }

                 if (profesja < 60) {
                 return document.getElementById("demo1").innerHTML = "Strażnik dróg";
               }
             
             
                 if (profesja < 62) {
                 return document.getElementById("demo1").innerHTML = "Strażnik więzienny";
               }
             
             
             
               if (profesja < 64) {
                 return document.getElementById("demo1").innerHTML = "Oficer";
               }
             
             
             
             
               if (profesja < 66) {
                 return document.getElementById("demo1").innerHTML = "Przewoźnik";
               }
             
             
             
               if (profesja < 68) {
                 return document.getElementById("demo1").innerHTML = "Rybak";
               }
             
             
               if (profesja < 70) {
                 return document.getElementById("demo1").innerHTML = "Rzezimieszek";
               }
             
             
             
               if (profesja < 72) {
                 return document.getElementById("demo1").innerHTML = "Skryba";
               }
             
             
             
               if (profesja < 74) {
                 return document.getElementById("demo1").innerHTML = "Sługa";
               }
             
             
               if (profesja < 76) {
                 return document.getElementById("demo1").innerHTML = "Szlachcic";
               }
             
             
               if (profesja < 78) {
                 return document.getElementById("demo1").innerHTML = "Śmieciarz";
               }
             
             
             
                    
               if (profesja < 80) {
                 return document.getElementById("demo1").innerHTML = "Rzemieślnik";
               }
                      
               if (profesja < 82) {
                 return document.getElementById("demo1").innerHTML = "Uczeń czarodzieja";
               }

                      
               if (profesja < 84) {
                 return document.getElementById("demo1").innerHTML = "Węglarz";
               }

                      
               if (profesja < 86) {
                 return document.getElementById("demo1").innerHTML = "Włóczykij";
               }

                      
               if (profesja < 88) {
                 return document.getElementById("demo1").innerHTML = "Woźnica";
               }

                      
               if (profesja < 90) {
                 return document.getElementById("demo1").innerHTML = "Zarządca";
               }
                      
               if (profesja < 92) {
                 return document.getElementById("demo1").innerHTML = "Złodziej";
               }
                      
               if (profesja < 94) {
                 return document.getElementById("demo1").innerHTML = "Żak";
               }
                      
               if (profesja < 96) {
                 return document.getElementById("demo1").innerHTML = "Żeglarz";
               }

                      
               if (profesja < 98) {
                 return document.getElementById("demo1").innerHTML = "Żołnierz";
               }
                      
               if (profesja < 100) {
                 return document.getElementById("demo1").innerHTML = "Żołnierz okrętowy";
               }

             
             
               return profesja;
             
             }
             </script>
             
             
             
             
<br><br><br>


             



             <p>Punkty życia </p>

             <button onclick="document.getElementById('demo3').innerHTML = kosczycia(1,10)">Losuj</button>
             
             <p id="demo3"></p>
             
             <script>
             function kosczycia(min, max) {
             
                 var zdrowie = Math.floor(Math.random() * 50) + 1;
                return zdrowie;    
                }
               </script> 
   
   
   
<br><br><br>




   <p>Wzrost </p>

   <button onclick="document.getElementById('demo4').innerHTML = koscwzrost(1,10)">Losuj</button>
   
   <p id="demo4"></p>
   
   <script>
   function koscwzrost(min, max) {
   
       var wzrost = Math.floor(Math.random() * 40) + 160;
      return wzrost;    
      }
     </script> 



<br><br><br>




<p>Waga w kg</p>

<button onclick="document.getElementById('demo6').innerHTML = koscwagi(1,10)">Losuj</button>

<p id="demo6"></p>

<script>
function koscwagi(min, max) {

    var waga = Math.floor(Math.random() * 100) + 1;




               if (waga < 10) {
                 return document.getElementById("demo6").innerHTML = " 35 kg ";
               }

               if (waga < 20) {
                 return document.getElementById("demo6").innerHTML = " 40 kg ";
               }

               if (waga < 30) {
                 return document.getElementById("demo6").innerHTML = " 45 kg ";
               }


              if (waga < 40) {
                 return document.getElementById("demo6").innerHTML = " 50 kg ";
               }


               if (waga < 50) {
                 return document.getElementById("demo6").innerHTML = " 60 kg ";
               }



               if (waga < 60) {
                 return document.getElementById("demo6").innerHTML = " 70 kg  ";
               }



               if (waga < 70) {
                 return document.getElementById("demo6").innerHTML = " 80 kg ";
               }



               if (waga < 80) {
                 return document.getElementById("demo6").innerHTML = " 90 kg ";
               }



               if (waga < 90) {
                 return document.getElementById("demo6").innerHTML = " 100 kg ";
               }




              if (waga < 100) {
                 return document.getElementById("demo6").innerHTML = " 110 kg ";
               }

               return waga;    
   }
  </script> 




<br><br><br>




<p>Kolor włosów</p>

<button onclick="document.getElementById('demo7').innerHTML = koscwłosów(1,10)">Losuj</button>

<p id="demo7"></p>

<script>
function koscwłosów(min, max) {

    var kolor = Math.floor(Math.random() * 100) + 1;


               
               if (kolor < 10) {
                 return document.getElementById("demo7").innerHTML = "Brązowy  ";
               }


               
               if (kolor < 20) {
                 return document.getElementById("demo7").innerHTML = " Brązowy ";
               }



               
               if (kolor <  30) {
                 return document.getElementById("demo7").innerHTML = " Jasnobrązowy ";
               }


               
               if (kolor < 40) {
                 return document.getElementById("demo7").innerHTML = " Jasnobrązowy ";
               }



               
               if (kolor < 50) {
                 return document.getElementById("demo7").innerHTML = " Rudy ";
               }



               
               if (kolor < 60) {
                 return document.getElementById("demo7").innerHTML = " Czarny ";
               }


               
               if (kolor < 70) {
                 return document.getElementById("demo7").innerHTML = " Popielaty ";
               }


               
               if (kolor < 80) {
                 return document.getElementById("demo7").innerHTML = " Blond ";
               }


               
               if (kolor < 90) {
                 return document.getElementById("demo7").innerHTML = "Ciemny blond  ";
               }



               
               if (kolor < 100) {
                 return document.getElementById("demo7").innerHTML = " Popielaty ";
               }

               return kolor;    
   }
  </script> 




<br><br><br>






<p>Kolor oczu</p>

<button onclick="document.getElementById('demo8').innerHTML = koscoczu(1,10)">Losuj</button>

<p id="demo8"></p>

<script>
function koscoczu(min, max) {

    var oczy = Math.floor(Math.random() * 100) + 1;


               
               if (oczy < 10) {
                 return document.getElementById("demo8").innerHTML = "Szary  ";
               }


               
               if (oczy < 20) {
                 return document.getElementById("demo8").innerHTML = " Ciemnoniebieski ";
               }



               
               if (oczy <  30) {
                 return document.getElementById("demo8").innerHTML = " Niebieski ";
               }


               
               if (oczy < 40) {
                 return document.getElementById("demo8").innerHTML = " Zielony ";
               }



               
               if (oczy < 50) {
                 return document.getElementById("demo8").innerHTML = " Piwny ";
               }



               
               if (oczy < 60) {
                 return document.getElementById("demo8").innerHTML = " Jasnobrązowy ";
               }


               
               if (oczy < 70) {
                 return document.getElementById("demo8").innerHTML = " Brązowy ";
               }


               
               if (oczy < 80) {
                 return document.getElementById("demo8").innerHTML = " Ciemnobrązowy ";
               }


               
               if (oczy < 90) {
                 return document.getElementById("demo8").innerHTML = "Fioletowy  ";
               }



               
               if (oczy < 100) {
                 return document.getElementById("demo8").innerHTML = " Czarny ";
               }

               return oczy;    
   }
  </script> 








<br><br><br>



<p>Znaki szczególne</p>

<button onclick="document.getElementById('demo9').innerHTML = koscoczu(1,10)">Losuj</button>

<p id="demo9"></p>

<script>
function koscoczu(min, max) {

    var szczegoly = Math.floor(Math.random() * 100) + 1;


                if (szczegoly < 5) {
                 return document.getElementById("demo9").innerHTML = " Blizna ";
               }



               if (szczegoly < 10) {
                 return document.getElementById("demo9").innerHTML = "Brak brwi  ";
               }


                if (szczegoly < 15) {
                 return document.getElementById("demo9").innerHTML = " Brak palca ";
               }



               if (szczegoly < 20) {
                 return document.getElementById("demo9").innerHTML = " Brak zęba ";
               }





               if (szczegoly < 25) {
                 return document.getElementById("demo9").innerHTML = " Brodawki ";
               }



               if (szczegoly < 30) {
                 return document.getElementById("demo9").innerHTML = " Blada cera ";
               }






               if (szczegoly < 35) {
                 return document.getElementById("demo9").innerHTML = " Duży nos ";
               }





               if (szczegoly < 40) {
                 return document.getElementById("demo9").innerHTML = " Duży pieprzyk ";
               }




               if (szczegoly < 45) {
                 return document.getElementById("demo9").innerHTML = " Dziwny zapach ciała ";
               }



               if (szczegoly < 50) {
                 return document.getElementById("demo9").innerHTML = " Kolczyk w nosie ";
               }


               
               if (szczegoly < 55) {
                 return document.getElementById("demo9").innerHTML = " Kolczyk w uchu ";
               }


               
               if (szczegoly < 60) {
                 return document.getElementById("demo9").innerHTML = " Niewielka łysina ";
               }



               
               if (szczegoly <  65) {
                 return document.getElementById("demo9").innerHTML = " Oczy różnego koloru ";
               }


               
               if (szczegoly < 70) {
                 return document.getElementById("demo9").innerHTML = " Piegi ";
               }



               
               if (szczegoly < 75) {
                 return document.getElementById("demo9").innerHTML = " Poszarpane ucho ";
               }



               
               if (szczegoly < 80) {
                 return document.getElementById("demo9").innerHTML = " Ślady po ospie ";
               }


               
               if (szczegoly < 85) {
                 return document.getElementById("demo9").innerHTML = " Tatuaż ";
               }


               
               if (szczegoly < 90) {
                 return document.getElementById("demo9").innerHTML = " Wystające zęby ";
               }


               
               if (szczegoly < 95) {
                 return document.getElementById("demo9").innerHTML = "Wytrzeszczone oczy  ";
               }



               
               if (szczegoly < 100) {
                 return document.getElementById("demo9").innerHTML = " Złamany nos ";
               }

               return szczegoly;    
   }
  </script> 







                <br><br><br>
        </div>

        <div  class="col-sm">
            <br>
            <div class="aktywniGracze" > </div>Zarejestrowani gracze: <br>


            @foreach($users as $user) 

<div class="zdobieniaDiva" style="font-size: 15px"> 
           <a href='{{route("getuser", $user->id)}}'> {!!$user->name!!} </a>
         
</div>


        @endforeach
 



        </div>


    </div>

</div>


<div class="container">
  
    <div class="row"> 
        
        <div  class="col-sm">
        <a href="<?php echo url('chat') ?>">  Non Clima Chat </a>
        </div>

        <div  class="col-sm">
  
        </div>

        <div  class="col-sm">
            <div class="wyloguj"> <a href="wylogowanie.blade.php"> Wyloguj się </a></div> 
        </div>
    </div>


</div>








    </body>
</html>
