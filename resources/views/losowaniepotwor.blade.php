


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    


        <script src="//cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>

        <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>


        <link rel="apple-touch-icon" sizes="180x180" href="/favicon.ico">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon.ico">  
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico">
        <link rel="manifest" href="/site.webmanifest">
        <!-- Styles -->
        <style>
        .zaloguj{
    color: chocolate;
}
.wyloguj{
color: chocolate;
}

.nawigacjaNawigacja{
font-weight: bold;

}
.nawigacjaWydarzenia{
    font-weight: bold;

}
.nawigacjaPrzygody{
    font-weight: bold;
 
}
.nawigacjaKarczma{
    font-weight: bold;

.nawigacjaWielkaBiblioteka{
    font-weight: bold;

}
.nawigacjaPostacie{
    font-weight: bold;

}

.tematTytul{
font-size: large;
font-weight: bold;
}
.tematTresc{
    
    font-weight: 600;
}
.graczPlayer{
font-style: italic;
}
.aktywniGracze{
    font-style: italic;

}

.tematData{
    font-style: italic;
}









.container{
            font-family: Brush Script MT, Brush Script Std, cursive; 
        }


        </style>
    </head>
    <body>
     


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">


<div class="container">
 
<div class="row">

        <div  class="col-sm">
            <a href="<?php echo url('/') ?>">
          <img src="obrazy/kompas1.png" style="width: 40%" href="index.html"></div>
            </a>

        <div  class="col-sm">

        <button type="button" class="button"> <a href="<?php echo url('newpost') ?>"> Dodaj nowy post </a> </button>
        </div>

        <div  class="col-sm">
        @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        </div>
 





</div>

</div>


<div class="container">
 
    <div class="row"> 
       
        <div  class="col-sm">
        <div class="nawigacjaNawigacja">  <a href="<?php echo url('/') ?>"> Nawigacja:</a></div>
            <br><br> 
            <div  class="nawigacjaWydarzenia"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('wydarzenia') ?>">  Wydarzenia</a></div><br>
           <div  class="nawigacjaPrzygody">   <img src="dragon.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('przygody') ?>">  Przygody</a> </div><br>
           <div  class="nawigacjaKarczma">    <img src="mermaid.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('karczma') ?>"> Karczma </a></div><br>
           <div  class="nawigacjaWielkaBiblioteka">  <img src="pumpkin-carriage.png" alt="Girl in a jacket" width="30px" height="30px">          <a href="<?php echo url('biblioteka') ?>">   Wielka Biblioteka</a></div><br>
           <div  class="nawigacjaPostacie">   <img src="hat.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listapostaci') ?>">     Postacie</a> </div><br>
           <div  class="nawigacjaPostacie">   <img src="werewolf.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listagraczy') ?>">     Gracze</a> </div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('obrazeniomat') ?>">  Obrazeniomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kostkomat') ?>">  Kostkomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('generatorpostaci') ?>">  Postaciomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kalkulator') ?>">  Kalkulator</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniedwor') ?>"> Losowanie dwór </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanielufy') ?>"> Losowanie lufy </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanieoblivion') ?>">Losowanie atrybutów  </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepopulacja') ?>"> Losowanie populacja</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepostac') ?>"> Losowanie postac</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepotwor') ?>"> Losowanie potwor</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanietechnologie') ?>"> Losowanie  technologie</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('okretomat') ?>"> Okrętomat</a></div><br>

           <br>
        </div>







        <div  class="col-sm">   <center>
            <p></p>
            <h1> <center> <b> Stworzony gatunek</b></center></h1>
            <p></p>
            
            
            
            
            
            
            
            <script> 
            document.write('<center> <b>Ilość nóg </b></center>');
            
            
                var i=0;  //zmienna podstawowa
                while (i<=0) //dziesięć razy powtarza pętlę
                {
                    let nogi = Math.floor(Math.random() * 10); 
                 //   {document.write('wywołana wartość:' + nogi +"<p></p>")}
                 
            
                    i++;
            
                     eksportNogi = nogi;
            
                //     new function (eksportNogi){
                //     document.write('udało się wczytać funkcję EKSPORTNOGI <p></p>');
                //     let testowaWartosc = eksportNogi * eksportNogi; 
                //     document.write('To testowa wartość przemnożona jako dwójnia eksportu ' + testowaWartosc + '<p></p>');
                //     document.write(testowaWartosc); 
                //     document.write('<p></p>');
                //     return eksportNogi
                // }
                  //  document.write('Eksportowana wartość :'+ eksportNogi + '<p></p>');
            
                    {document.write('');}
                    if (nogi <= 1)
                    {document.write('brak');}
                
                    else if (nogi <= 2){
                        document.write('2');
                    }
                    
                    else if (nogi <= 3){
                        document.write('4');
                    }  
                    else if (nogi <= 4){
                        document.write('6');
                    }  
                    else if (nogi <= 5){
                        document.write('8');
                    }  
                    else if (nogi <= 6){
                        document.write('10');
                    }  
                    else if (nogi <= 7){
                        document.write(' 12 ');
                    }  
                    else if (nogi <= 8){
                        document.write('2');
                    }  
                    else if (nogi <= 9){
                        document.write('2');
                    }  
                    else if (nogi <= 10){
                        document.write('2');
                    }  
                    
                
            }
            
            
            
            
            
               // {document.write('wywołana wartość:' + nogi +"<p></p>")}
                //  document.write('eksportowane nogi :' + eksportNogi);
                document.write('<p></p><b><center>Ilość rąk</center></b>');
            
            
            
            
            
              var i=0;  //zmienna podstawowa
                while (i<=0) //dziesięć razy powtarza pętlę
                {
                    let ruki = Math.floor(Math.random() * 10); 
                    i++;
                    let eksportRuki = ruki;
                 //   document.write('Eksportowana wartość :'+ eksportRuki + '<p></p>');
            
            
                    {document.write('');}
                    if (ruki <= 1)
                    {document.write('brak');}
                
                    else if (ruki <= 2){
                        document.write('2 ');
                    }
                    
                    else if (ruki <= 3){
                        document.write('4 ');
                    }  
                    else if (ruki <= 4){
                        document.write('6');
                    }  
                    else if (ruki <= 5){
                        document.write('8');
                    }  
                    else if (ruki <= 6){
                        document.write('10');
                    }  
                    else if (ruki <= 7){
                        document.write(' 12 ');
                    }  
                    else if (ruki <= 8){
                        document.write('2');
                    }  
                    else if (ruki <= 9){
                        document.write('2');
                    }  
                    else if (ruki <= 10){
                        document.write('2');
                    }  
                    
                }
            
              
                document.write('<p></p><b><center>Postać</center></b>');
            
            
            
            
            
            
            
            
              var i=0;  //zmienna podstawowa
                while (i<=0) //dziesięć razy powtarza pętlę
                {
                    let postac = Math.floor(Math.random() * 10); 
                    i++;
                    let eksportPostac = postac;
            //        document.write('Eksportowana wartość :'+ eksportPostac + '<p></p>');
            
                    {document.write('');}
                    if (postac <= 1)
                    {document.write('Śluz');}
                
                    else if (postac <= 2){
                        document.write('Ciało');
                    }
                    
                    else if (postac <= 3){
                        document.write('Ciało');
                    }  
                    else if (postac <= 4){
                        document.write('Ciało');
                    }  
                    else if (postac <= 5){
                        document.write('Bio-robot');
                    }  
                    else if (postac <= 6){
                        document.write('Pół-niematerialna');
                    }  
                    else if (postac <= 7){
                        document.write('Niematerialna');
                    }  
                    else if (postac <= 8){
                        document.write('Mechaniczna');
                    }  
                    else if (postac <= 9){
                        document.write('Elektryczna');
                    }  
                    else if (postac <= 10){
                        document.write('Zmiennokształtna');
                    }  
                    
                }
            
            
                document.write('<p></p><b><center>Sposób poruszania się:</center></b>');
            
            
            
            
            
            
            
            
             var i=0;  //zmienna podstawowa
                while (i<=0) //dziesięć razy powtarza pętlę
                {
                    let naped = Math.floor(Math.random() * 10); 
                    i++;
                    let eksportNaped = naped;
                  //  document.write('Eksportowana wartość :'+ eksportNaped + '<p></p>');
            
                    {document.write('');}
                    if (naped <= 1)
                    {document.write('Chodzenie');}
                
                    else if (naped <= 2){
                        document.write('Pływanie');
                    }
                    
                    else if (naped <= 3){
                        document.write('Chodzenie');
                    }  
                    else if (naped <= 4){
                        document.write('Pływanie');
                    }  
                    else if (naped <= 5){
                        document.write('Latanie');
                    }  
                    else if (naped <= 6){
                        document.write('Teleportacja');
                    }  
                    else if (naped <= 7){
                        document.write(' Oczy duszy');
                    }  
                    else if (naped <= 8){
                        document.write(' Pojazdy mechaniczne');
                    }  
                    else if (naped <= 9){
                        document.write('Inne stworzenia');
                    }  
                    else if (naped <= 10){
                        document.write('Chodzenie');
                    }  
                    
                }
            
            
            
                document.write('<p></p><b><center>Wielkość stworzenia:</center></b>');
            
            
            
            
            
             var i=0;  //zmienna podstawowa
                while (i<=0) //dziesięć razy powtarza pętlę
                {
                    let wielkosc = Math.floor(Math.random() * 10); 
                    i++;
                    let eksportWielkosc = wielkosc;
               //    document.write('Eksportowana wartość :'+ eksportWielkosc + '<p></p>');
            
                    {document.write('');}
                    if (wielkosc <= 1)
                    {document.write('Maleńki  - 1-20 cm');}
                
                    else if (wielkosc <= 2){
                        document.write('Mały  - 21-60 cm');
                    }
                    
                    else if (wielkosc <= 3){
                        document.write('Nieduży  - 61-120 cm ');
                    }  
                    else if (wielkosc <= 4){
                        document.write('Ludzki  - 121-240 cm');
                    }  
                    else if (wielkosc <= 5){
                        document.write('Duży  - 2,5 metra- 5 metrów ');
                    }  
                    else if (wielkosc <= 6){
                        document.write('Ogromny  - 6 metrów- 12 metrów');
                    }  
                    else if (wielkosc <= 7){
                        document.write('Gigantyczny  - 13-24 metrów  ');
                    }  
                    else if (wielkosc <= 8){
                        document.write('Gargantuniczny  - 25-50  metrów');
                    }  
                    else if (wielkosc <= 9){
                        document.write('Epicki  - 51-100 metrow');
                    }  
                    else if (wielkosc <= 10){
                        document.write('Monstrualny  - 101-200 metrów');
                    }  
                    
                }
            
                document.write('<p></p><b><center>Rodzaj skóry:</center></b>');
            
            
            
            
            
            
            var i=0;  //zmienna podstawowa
                while (i<=0) //dziesięć razy powtarza pętlę
                {
                    let skora = Math.floor(Math.random() * 10); 
                    i++;
                    let eksportSkora = skora;
                //    document.write('Eksportowana wartość :'+ eksportSkora + '<p></p>');
            
                    {document.write('');}
                    if (skora <= 1)
                    {document.write('Zwykła (skóra + włosy)');}
                
                    else if (skora <= 2){
                        document.write('Futro');
                    }
                    
                    else if (skora <= 3){
                        document.write('Pióra');
                    }  
                    else if (skora <= 4){
                        document.write('Łuski');
                    }  
                    else if (skora <= 5){
                        document.write('Chitynowa');
                    }  
                    else if (skora <= 6){
                        document.write('Śluz');
                    }  
                    else if (skora <= 7){
                        document.write('Skamieniała');
                    }  
                    else if (skora <= 8){
                        document.write('Brak');
                    }  
                    else if (skora <= 9){
                        document.write('Drewno');
                    }  
                    else if (skora <= 10){
                        document.write('Zwykła skóra');
                    }  
                    
                }
            
            
            
            
            
                document.write('<p></p><b><center>Sposób oddychania</center></b>');
            
            
            
            
            
            
            
            
            var i=0;  //zmienna podstawowa
                while (i<=0) //dziesięć razy powtarza pętlę
                {
                    let oddychanie = Math.floor(Math.random() * 10); 
                    i++;
                    let eksportOddychanie = oddychanie;
             //       document.write('Eksportowana wartość :'+ eksportOddychanie + '<p></p>');
            
                    {document.write('');}
                    if (oddychanie <= 1)
                    {document.write('Powietrze');}
                
                    else if (oddychanie <= 2){
                        document.write('Woda');
                    }
                    
                    else if (oddychanie <= 3){
                        document.write('Prana');
                    }  
                    else if (oddychanie <= 4){
                        document.write('Nie oddycha');
                    }  
                    else if (oddychanie <= 5){
                        document.write('Fotosynteza');
                    }  
                    else if (oddychanie <= 6){
                        document.write('Powietrze');
                    }  
                    else if (oddychanie <= 7){
                        document.write(' Powietrze ');
                    }  
                    else if (oddychanie <= 8){
                        document.write('Woda');
                    }  
                    else if (oddychanie <= 9){
                        document.write('Utlenianie płynów');
                    }  
                    else if (oddychanie <= 10){
                        document.write('Fotosyntaza');
                    }  
                    
                }
            
             
                document.write('<p></p><b><center>Naturalne środowiska:</center></b>');
            
            
            
            
            
            
            
            
            var i=0;  //zmienna podstawowa
                while (i<=0) //dziesięć razy powtarza pętlę
                {
                    let srodowisko = Math.floor(Math.random() * 10); 
                    i++;
                    let   eksportSrodowisko = srodowisko;
               //     document.write('Eksportowana wartość :'+ eksportSrodowisko + '<p></p>');
            
                    {document.write('');}
                    if (srodowisko <= 1)
                    {document.write('Woda - zimna ');}
                
                    else if (srodowisko <= 2){
                        document.write('Woda  - letnia  ');
                    }
                    
                    else if (srodowisko <= 3){
                        document.write('Woda - gorąca ');
                    }  
                    else if (srodowisko <= 4){
                        document.write('Ląd - zimny');
                    }  
                    else if (srodowisko <= 5){
                        document.write('Ląd - letni');
                    }  
                    else if (srodowisko <= 6){
                        document.write('Ląd - ciepły');
                    }  
                    else if (srodowisko <= 7){
                        document.write(' Atmosfera ');
                    }  
                    else if (srodowisko <= 8){
                        document.write('Podziemia');
                    }  
                    else if (srodowisko <= 9){
                        document.write('Astral');
                    }  
                    else if (srodowisko <= 10){
                        document.write('Ląd uniwersalny');
                    }  
                    
                }
            
            
            
            
             
                document.write('<p></p><b><center>Moc rasowa:</center></b>');
            
            
            
            
            
            
            
            var i=0;  //zmienna podstawowa
            while (i<=0) //dziesięć razy powtarza pętlę
            {
                let moc = Math.floor(Math.random() * 10); 
                i++;
                let  eksportMoc = moc;
            //    document.write('Eksportowana wartość :'+ eksportMoc + '<p></p>');
            
                {document.write('');}
                if (moc <= 1)
                {document.write('Wrażliwość astralna  (Moc magiczna podwójnie zwiększona, lecz kosztem podwójnej podatności na magię.)');}
            
                else if (moc <= 2){
                    document.write('Pochłaniacz mocy astralnych. (Połowę mocy magicznej jest absorbowane przez Ciebie, dodatkowo masz więcej potężny bonus bazowy magii. Kosztem jest brak samoczynnej regeneracji. ');
                }
                
                else if (moc <= 3){
                    document.write('Czytanie myśli. (Potrafią czytać myśli - co nie oznacza że zawsze je rozumieją. ) ');
                }  
                else if (moc <= 4){
                    document.write('Talent do języków. (Mistrzostwo lingwistyczne tej rasy jest szeroko znane.');
                }  
                else if (moc <= 5){
                    document.write('Naród wybrany. (Handel, szpiegostwo i uczoność, kosztem powszechnych uprzedzeń');
                }  
                else if (moc <= 6){
                    document.write('Umysł roju. (Rasa ma swój umysł roju, który pozwala doskonale współpracować). ');
                }  
                else if (moc <= 7){
                    document.write(' Twardy gatunek. (Potężnie zwiększa fizyczną odporność). ');
                }  
                else if (moc <= 8){
                    document.write(' Potężna siła. (Rasa ma potężną siłę.');
                }  
                else if (moc <= 9){
                    document.write(' Tworcy kultury. (Każdy członek rasy ma wyjątkowe zdolności artystyczne, przynajmniej w porówianiu z innymi rasami.');
                }  
                else if (moc <= 10){
                    document.write('Uniwersalność. Rasa jest uniwersalna, powszechnie obecna i powszechnie akceptowana, niezależnie od planety.');
                }  
                
            }        
            //{document.write('wywołana wartość:' + nogi +"<p></p>")}
            
            
            //pokazNogi = nogi;
            //document.write(pokazNogi);
            //pokazRuki = ruki;
            //document.write(pokazRuki);
            
            </script>
            
            
            
            <!--
            <b><h2>Ilosc zjadanego jedzenia: </h2> </b>
            -->
            
            
            <script>
                
            /*
            
            
             if (nogi <=1,  ruki <=1, postac <=1, naped <=1, wielosc <=1, skora <=1, oddychanie <=1, srodowisko <=1, moc <= 1)
                {document.write('');}
            
            
                else if (nogi <=2,  ruki <=2, postac <=2, naped <=2, wielosc <=2, skora <=2, oddychanie <=2, srodowisko <=2, moc <= 2)
                {document.write('');}
            
            
                else if (nogi <=3,  ruki <=3, postac <=3, naped <=3, wielosc <=3, skora <=3, oddychanie <=3, srodowisko <=1, moc <= 1)
                {document.write('');}
            
            
                else if (nogi <=1,  ruki <=1, postac <=1, naped <=1, wielosc <=1, skora <=1, oddychanie <=1, srodowisko <=1, moc <= 1)
                {document.write('');}
            
            
                else if (nogi <=1,  ruki <=1, postac <=1, naped <=1, wielosc <=1, skora <=1, oddychanie <=1, srodowisko <=1, moc <= 1)
                {document.write('');}
            
            
            
                else if (nogi <=1,  ruki <=1, postac <=1, naped <=1, wielosc <=1, skora <=1, oddychanie <=1, srodowisko <=1, moc <= 1)
                {document.write('');}
            
            
            
                else if (nogi <=1,  ruki <=1, postac <=1, naped <=1, wielosc <=1, skora <=1, oddychanie <=1, srodowisko <=1, moc <= 1)
                {document.write('');}
            
            
            
                else if (nogi <=1,  ruki <=1, postac <=1, naped <=1, wielosc <=1, skora <=1, oddychanie <=1, srodowisko <=1, moc <= 1)
                {document.write('');}
            
            
            
                else if (nogi <=1,  ruki <=1, postac <=1, naped <=1, wielosc <=1, skora <=1, oddychanie <=1, srodowisko <=1, moc <= 1)
                {document.write('');}
            
            
            let kalorieNogi = 0;
            let kalorieRuki = 0;
            let kaloriePostac = 0;
            let kalorieNaped = 0;
            let kalorieWielkosc = 0; 
            let kalorieSkora = 0;
            let kalorieOddychanie = 0;
            let kalorieSrodowisko = 0; 
            let kalorieMoc = 0;
            
            
            var i=0;  //zmienna podstawowa
            while (i<=0) //dziesięć razy powtarza pętlę
            {   
                if (nogi <= 1){
                    let kalorieNogi = 10;
                }
            
                else if (nogi <=2){
                    let kalorieNogi = 20;
                }
                else if (nogi <=3){
                    let kalorieNogi = 40;
                }
                else if (nogi <=4){
                    let kalorieNogi = 60;
                }
                else if (nogi <=5){
                    let kalorieNogi = 60;
                }
                else if (nogi <=6){
                    let kalorieNogi = 100;
                }
                else if (nogi <=7){
                    let kalorieNogi = 120;
                }
                else if (nogi <=8){
                    let kalorieNogi = 20;
                }
                else if (nogi <=9){
                    let kalorieNogi = 20;
                }
                else if (nogi <=10){
                    let kalorieNogi = 20;
                }
            }
            
            var i=0;  //zmienna podstawowa
            while (i<=0) //dziesięć razy powtarza pętlę
            {   
                if (ruki <= 1){
                    let kalorieRuki = 10;
                }
            
                else if (ruki <=2){
                    let kalorieRuki = 20;
                }
                else if (ruki <=3){
                    let kalorieRuki = 40;
                }
                else if (ruki <=4){
                    let kalorieRuki = 60;
                }
                else if (ruki <=5){
                    let kalorieRuki = 60;
                }
                else if (ruki <=6){
                    let kalorieRuki = 100;
                }
                else if (ruki <=7){
                    let kalorieRuki = 120;
                }
                else if (ruki <=8){
                    let kalorieRuki = 20;
                }
                else if (ruki <=9){
                    let kalorieRuki = 20;
                }
                else if (ruki <=10){
                    let kalorieRuki = 20;
                }
            }
            
            var i=0;  //zmienna podstawowa
            while (i<=0) //dziesięć razy powtarza pętlę
            {   
                if (skora <= 1){
                    let kalorieSkora = 20;
                }
            
                else if (skora <=2){
                    let kalorieSkora = 20;
                }
                else if (skora <=3){
                    let kalorieSkora = 20;
                }
                else if (skora <=4){
                    let kalorieSkora = 20;
                }
                else if (skora <=5){
                    let kalorieSkora = 20;
                }
                else if (skora <=6){
                    let kalorieSkora = 20;
                }
                else if (skora <=7){
                    let kalorieSkora = 20;
                }
                else if (skora <=8){
                    let kalorieSkora = 20;
                }
                else if (skora <=9){
                    let kalorieSkora = 20;
                }
                else if (skora <=10){
                    let kalorieSkora = 20;
                }
            }
            
            
            
            
            
            var i=0;  //zmienna podstawowa
            while (i<=0) //dziesięć razy powtarza pętlę
            {   
                if (wielkosc <= 1){
                    let kalorieWielkosc = 1;
                }
            
                else if (wielkosc <=2){
                    let kalorieWielkosc = 10;
                }
                else if (wielkosc <=3){
                    let kalorieWielkosc = 100;
                }
                else if (wielkosc <=4){
                    let kalorieWielkosc = 1000;
                }
                else if (wielkosc <=5){
                    let kalorieWielkosc = 10000;
                }
                else if (wielkosc <=6){
                    let kalorieWielkosc = 100000;
                }
                else if (wielkosc <=7){
                    let kalorieWielkosc = 1000000;
                }
                else if (wielkosc <=8){
                    let kalorieWielkosc = 10000000;
                }
                else if (wielkosc <=9){
                    let kalorieWielkosc = 100000000;
                }
                else if (wielkosc <=10){
                    let kalorieWielkosc = 1000000000;
                }
            }
            
            
            
            
            
            
            
            var i=0;  //zmienna podstawowa
            while (i<=0) //dziesięć razy powtarza pętlę
            {   
                if (oddychanie <= 1){
                    let kalorieOddychanie = 20;
                }
            
                else if (oddychanie <=2){
                    let kalorieOddychanie = 20;
                }
                else if (oddychanie <=3){
                    let kalorieOddychanie = 20;
                }
                else if (oddychanie <=4){
                    let kalorieOddychanie = 20;
                }
                else if (oddychanie <=5){
                    let kalorieOddychanie = 20;
                }
                else if (oddychanie <=6){
                    let kalorieOddychanie = 20;
                }
                else if (oddychanie <=7){
                    let kalorieOddychanie = 20;
                }
                else if (oddychanie <=8){
                    let kalorieOddychanie = 20;
                }
                else if (oddychanie <=9){
                    let kalorieOddychanie = 20;
                }
                else if (oddychanie <=10){
                    let kalorieOddychanie = 20;
                }
            }
            
            
            
            
            
            
            
            var i=0;  //zmienna podstawowa
            while (i<=0) //dziesięć razy powtarza pętlę
            {   
                if (srodowisko <= 1){
                    let kalorieSrodowisko = 50;
                }
            
                else if (srodowisko <=2){
                    let kalorieSrodowisko = 30;
                }
                else if (srodowisko <=3){
                    let kalorieSrodowisko = 20;
                }
                else if (srodowisko <=4){
                    let kalorieSrodowisko = 60;
                }
                else if (srodowisko <=5){
                    let kalorieSrodowisko = 40;
                }
                else if (srodowisko <=6){
                    let kalorieSrodowisko = 30;
                }
                else if (srodowisko <=7){
                    let kalorieSrodowisko = 20;
                }
                else if (srodowisko <=8){
                    let kalorieSrodowisko = 100;
                }
                else if (srodowisko <=9){
                    let kalorieSrodowisko = 100;
                }
                else if (srodowisko <=10){
                    let kalorieSrodowisko = 60;
                }
            }
            
            
            
            
            
            
            
            
            var i=0;  //zmienna podstawowa
            while (i<=0) //dziesięć razy powtarza pętlę
            {   
                if (moc <= 1){
                    let kalorie = 1;
                }
            
                else if (moc <=2){
                    let kalorie = 1;
                }
                else if (moc <=3){
                    let kalorie = 1;
                }
                else if (moc <=4){
                    let kalorie = 1;
                }
                else if (moc <=5){
                    let kalorie = 1;
                }
                else if (moc <=6){
                    let kalorie = 1;
                }
                else if (moc <=7){
                    let kalorie = 1;
                }
                else if (moc <=8){
                    let kalorie = 1;
                }
                else if (moc <=9){
                    let kalorie = 1;
                }
                else if (moc <=10){
                    let kalorie = 1;
                }
            }
            
            
            
            
            
            
            let potrzebny_pokarm = (kalorieNogi + kalorieRuki + kaloriePostac + kalorieNaped + kalorieSkora + kalorieOddychanie + kalorieSrodowisko + kalorieMoc) * kalorieWielkosc; 
            document.write(potrzebny_pokarm);
            */
            </script>
            
            
            
            </center>  </div>




        

<div class="container">
  
    <div class="row"> 
        
        <div  class="col-sm">
        <a href="<?php echo url('chat') ?>">  Non Clima Chat </a>
        </div>

        <div  class="col-sm">
         
        </div>

        <div  class="col-sm">
            <div class="wyloguj"> <a href="wylogowanie.blade.php"> Wyloguj się </a></div> 
        </div>
    </div>


</div>








    </body>
</html>
