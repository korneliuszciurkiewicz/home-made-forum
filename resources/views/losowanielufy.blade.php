


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    


        <script src="//cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>

        <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>


        <link rel="apple-touch-icon" sizes="180x180" href="/favicon.ico">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon.ico">  
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico">
        <link rel="manifest" href="/site.webmanifest">
        <!-- Styles -->
        <style>
        .zaloguj{
    color: chocolate;
}
.wyloguj{
color: chocolate;
}

.nawigacjaNawigacja{
font-weight: bold;

}
.nawigacjaWydarzenia{
    font-weight: bold;

}
.nawigacjaPrzygody{
    font-weight: bold;
 
}
.nawigacjaKarczma{
    font-weight: bold;

.nawigacjaWielkaBiblioteka{
    font-weight: bold;

}
.nawigacjaPostacie{
    font-weight: bold;

}

.tematTytul{
font-size: large;
font-weight: bold;
}
.tematTresc{
    
    font-weight: 600;
}
.graczPlayer{
font-style: italic;
}
.aktywniGracze{
    font-style: italic;

}

.tematData{
    font-style: italic;
}









.container{
            font-family: Brush Script MT, Brush Script Std, cursive; 
        }


        </style>
    </head>
    <body>
     


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">


<div class="container">
 
<div class="row">

        <div  class="col-sm">
            <a href="<?php echo url('/') ?>">
          <img src="obrazy/kompas1.png" style="width: 40%" href="index.html"></div>
            </a>

        <div  class="col-sm">

        <button type="button" class="button"> <a href="<?php echo url('newpost') ?>"> Dodaj nowy post </a> </button>
        </div>

        <div  class="col-sm">
        @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        </div>
 





</div>

</div>


<div class="container">
 
    <div class="row"> 
       
        <div  class="col-sm">
        <div class="nawigacjaNawigacja">  <a href="<?php echo url('/') ?>"> Nawigacja:</a></div>
            <br><br> 
            <div  class="nawigacjaWydarzenia"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('wydarzenia') ?>">  Wydarzenia</a></div><br>
           <div  class="nawigacjaPrzygody">   <img src="dragon.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('przygody') ?>">  Przygody</a> </div><br>
           <div  class="nawigacjaKarczma">    <img src="mermaid.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('karczma') ?>"> Karczma </a></div><br>
           <div  class="nawigacjaWielkaBiblioteka">  <img src="pumpkin-carriage.png" alt="Girl in a jacket" width="30px" height="30px">          <a href="<?php echo url('biblioteka') ?>">   Wielka Biblioteka</a></div><br>
           <div  class="nawigacjaPostacie">   <img src="hat.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listapostaci') ?>">     Postacie</a> </div><br>
           <div  class="nawigacjaPostacie">   <img src="werewolf.png" alt="Girl in a jacket" width="30px" height="30px">       <a href="<?php echo url('listagraczy') ?>">     Gracze</a> </div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('obrazeniomat') ?>">  Obrazeniomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kostkomat') ?>">  Kostkomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('generatorpostaci') ?>">  Postaciomat</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('kalkulator') ?>">  Kalkulator</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniedwor') ?>"> Losowanie dwór </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanielufy') ?>"> Losowanie lufy </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanieoblivion') ?>">Losowanie atrybutów  </a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepopulacja') ?>"> Losowanie populacja</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepostac') ?>"> Losowanie postac</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowaniepotwor') ?>"> Losowanie potwor</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('losowanietechnologie') ?>"> Losowanie  technologie</a></div><br>
           <div  class="nawigacjaPostacie"> <img src="castle.png" alt="Girl in a jacket" width="30px" height="30px">   <a href="<?php echo url('okretomat') ?>"> Okrętomat</a></div><br>

           <br>
        </div>








        <div  class="col-sm">


Wprowadź kaliber pocisku w milimetrach: <input type="text" id="kaliber">

<br><button onclick="wykujDzialo()">Zatwierdź kaliber</button> <br>
<br><br><br>






            <script>
             function wykujDzialo(){
                 
                    let kaliber = document.getElementById("kaliber").value;
                    let koszt_pocisku = (kaliber*15);
                    let price = (kaliber*5000);
                    let waga = kaliber*kaliber/3;
                    let waga_pocisku = kaliber*0.4;
                    let zasieg_zniszczen_umocnien = kaliber*0.01;
                    let zasieg_zniszczen_budynkow = kaliber*0.1;
                    let zasieg_zniszczen_ludzi = kaliber*0.8;
                    let zasieg_zniszczen_okien = kaliber*3;
                    let zasieg_strzalu = kaliber *110;

                    document.write('Cena działa:'+price+ ' $'+'<br><br>');
                    document.write('Koszt pocisku:'+koszt_pocisku+ ' $'+'<br><br>');
                    document.write('Kaliber:'+kaliber+ ' mm'+'<br><br>') ;
                    document.write('Waga:'+waga + ' kg'+'<br><br>');
                    document.write('Waga pocisku:'+waga_pocisku + ' kg'+'<br><br>'); 
                    document.write('Zasięg strzału:'+zasieg_strzalu + ' m'+'<br><br>'); 
                    document.write('Zniszczenia umocnień betonowych:'+zasieg_zniszczen_umocnien+ ' m'+'<br><br>') ;
                    document.write('Zniszczenia budynków:'+zasieg_zniszczen_budynkow+ ' m'+'<br><br>') ;
                    document.write('Zniszczenia istot odsłoniętych:'+zasieg_zniszczen_ludzi+ ' m'+'<br><br>');
                    document.write('Zniszczenia okien:'+zasieg_zniszczen_okien+ ' m'+'<br><br>') ;
                        
             }
     </script>











Wprowadź wagę bomby w kilogramach: <input type="text" id="waga">

<br><button onclick="wykujBombe()">Zatwierdź bombę</button> <br>
<br><br><br>

            <script>
             function wykujBombe(){
                 
                    let waga = document.getElementById("waga").value;
                    let koszt_pocisku = (waga*500);
                    let zasieg_zniszczen_umocnien = waga*0.05;
                    let zasieg_zniszczen_budynkow = waga*0.3;
                    let zasieg_zniszczen_ludzi = waga*0.7;
                    let zasieg_zniszczen_okien = waga*3;

                  
                    document.write('Koszt bomby'+koszt_pocisku+ ' $'+'<br><br>');
                    document.write('Waga:'+waga + ' kg'+'<br><br>');
                    document.write('Zniszczenia umocnień betonowych:'+zasieg_zniszczen_umocnien+ ' m'+'<br><br>') ;
                    document.write('Zniszczenia budynków:'+zasieg_zniszczen_budynkow+ ' m'+'<br><br>') ;
                    document.write('Zniszczenia istot odsłoniętych:'+zasieg_zniszczen_ludzi+ ' m'+'<br><br>');
                    document.write('Zniszczenia okien:'+zasieg_zniszczen_okien+ ' m'+'<br><br>') ;
                        
             }

     </script>













Wprowadź wagę bomby w kilotonach: <input type="text" id="kilotony">

<br><button onclick="wykujAtoma()">Zatwierdź atomówkę</button> <br>
<br><br><br>

            <script>
             function wykujAtoma(){
                 
                    let kilotony = document.getElementById("kilotony").value;
                    let waga = kilotony * 6
                    let koszt_pocisku = (kilotony*500000);
                    let zasieg_zniszczen_umocnien = kilotony*80;
                    let zasieg_zniszczen_budynkow = kilotony*260;
                    let zasieg_zniszczen_ludzi = kilotony*900;
                    let zasieg_zniszczen_okien = kilotony*6000;

                  
                    document.write('Koszt bomby'+koszt_pocisku+ ' $'+'<br><br>');
                    document.write('Waga:'+kilotony + ' ton'+'<br><br>');
                    document.write('Zniszczenia umocnień betonowych:'+zasieg_zniszczen_umocnien+ ' m'+'<br><br>') ;
                    document.write('Zniszczenia budynków:'+zasieg_zniszczen_budynkow+ ' m'+'<br><br>') ;
                    document.write('Zniszczenia istot odsłoniętych:'+zasieg_zniszczen_ludzi+ ' m'+'<br><br>');
                    document.write('Zniszczenia okien:'+zasieg_zniszczen_okien+ ' m'+'<br><br>') ;
                        
             }
             
     </script>













    <div class="glowny">










    Tutaj bedzie generator armat.
    <p>
    </p>
    


 


<br>














      </div>




        

<div class="container">
  
    <div class="row"> 
        
        <div  class="col-sm">
        <a href="<?php echo url('chat') ?>">  Non Clima Chat </a>
        </div>

        <div  class="col-sm">
         
        </div>

        <div  class="col-sm">
            <div class="wyloguj"> <a href="wylogowanie.blade.php"> Wyloguj się </a></div> 
        </div>
    </div>


</div>








    </body>
</html>
