<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\User;
use App\Odpowiedzi;
use App\Czat;

class Category extends Model
{
    public function getOdpowiedzi()
    {
        return $this->hasMany('App\Odpowiedzi', 'id_posta', 'id');
        
    }
}
