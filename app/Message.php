<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\User;
use App\Odpowiedzi;
use App\Czat;





class Message extends Model
{
    protected $fillable = ['message'];
    
    
}


Schema::create('messages', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('user_id')->unsigned();
    $table->text('message');
    $table->timestamps();
  });


  