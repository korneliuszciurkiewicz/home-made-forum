<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\User;
use App\Odpowiedzi;
use App\Czat;

class KartaPostaci extends Model
{
    protected $table = 'kartypostaci';
}
