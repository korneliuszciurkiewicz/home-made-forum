<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\KartaPostaci;
use App\User;
use App\Odpowiedzi;

class ObrazeniomatController extends Controller
{
  /**

     *
    * @return Response
     */

    public function index()
    {

        $users = \App\User::all();
        
        $data = array();
        $data['users'] = $users;
        return view('obrazeniomat', $data);


        
    }

}
