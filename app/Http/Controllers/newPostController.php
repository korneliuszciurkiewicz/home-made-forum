<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;

class newPostController extends Controller
{
        /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    public function index()
    {
        $categories = DB::table('categories')->get();

        return view('category.index', ['categories' => $categories]);
    }



    public function create()
    {
        $posts = \App\Category::all();
        $users = \App\User::all();
        $data = array();

        $data['posts'] = $posts;
        $data['users'] = $users;

        return view('newpost', $data);
    }


    public function store(Request $request)
    {
        $cat_name = $request->input('cat_name');
        $conentPost = $request->input('conentPost');
        $dzial = $request->input('dzial');
        $autor = $request->input('autor');
        $category = new Category();
        $category -> titlePost = $cat_name; 
        $category -> conentPost = $conentPost; 
        $category -> dzial = $dzial;
        $category -> autor = $autor;
        $category -> save();
    }
}








