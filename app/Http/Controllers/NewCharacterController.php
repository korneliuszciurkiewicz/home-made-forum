<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\KartaPostaci;
use App\User;
use App\Odpowiedzi;


class NewCharacterController extends Controller
{
        /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */





    public function index()
    {
        $posts = \App\Category::where('dzial', 'Postacie' )
        ->orderBy('id', 'DESC') 
        ->get(); 



        $users = \App\User::all();
        $kartypostaci = \App\KartaPostaci::all();

        $data = array();

        $data['posts'] = $posts;
        $data['users'] = $users;
        $data['kartypostaci'] = $kartypostaci;
 
        $categories = DB::table('categories')->get();
        $kartypostaci = DB::table('kartypostaci')->get();


        return view('nowapostac', $data);


        
    }




    public function store(Request $request)
    {
        
      $kartypostaci = new \App\KartaPostaci();
        $postac = $request->input('postac');
        $opis = $request->input('opis');
        $tajemne = $request->input('tajemne');
        $avatar = $request->input('avatar');
        $rasapostaci = $request->input('rasaPostaci');


        $kartypostaci -> postac = $postac;
        $kartypostaci -> opis = $opis;
        $kartypostaci -> tajemne = $tajemne;
        $kartypostaci -> avatar = $avatar;
        $kartypostaci -> rasaPostaci = $rasapostaci;

        $kartypostaci -> save();
    }

}
