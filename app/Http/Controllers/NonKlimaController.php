<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\User;
use App\KartaPostaci;

class RejestracjaController extends Controller
{
    public function index()
    {
        $user = \App\User::all();
        $users = \App\User::all();
        $data = array();
 
        $data['users'] = $users;
        $data['user'] = $user;
        return view('nonklima', $data);
    }
}
